package com.gudanggaramtbk.myggchecker.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NavUtils;
import android.Manifest;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.TextView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.gudanggaramtbk.listofvalue.ListofValue;
import com.gudanggaramtbk.listofvalue.model.CLovProperty;
import com.gudanggaramtbk.listofvalue.model.CSoapConfig;
import com.gudanggaramtbk.myggchecker.R;
import com.gudanggaramtbk.myggchecker.model.CBCodeBinAssign;
import com.gudanggaramtbk.myggchecker.model.CBCodeInfo;
import com.gudanggaramtbk.myggchecker.service.SoapPopulateBatchNo;
import com.gudanggaramtbk.myggchecker.service.SoapPopulateInfo;
import com.gudanggaramtbk.myggchecker.service.SoapPutBarcodeToBin;
import com.gudanggaramtbk.myggchecker.util.ImageHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import android.widget.Toast;
import org.ksoap2.serialization.PropertyInfo;

public class BinAssignActivity extends AppCompatActivity {
    private ImageView          Img_ps_1;
    private ImageView          Img_ps_2;
    private ImageView          Img_ps_3;
    Bitmap                     BlankImage;
    private ImageHelper        ihx;
    private SharedPreferences  config;
    private Context            context;
    private List<CBCodeInfo>   Result;
    private String             username;
    private String             userId;
    private String             mfg_org_id;
    private String             mfg_org_name;
    private Boolean            ScanState;

    TextView Txt_transaction_number;
    TextView Txt_item_code;
    TextView Txt_barcode_num;
    TextView Txt_lot_number;
    TextView Txt_no_koli;
    TextView Txt_qty;
    TextView Txt_uom_code;
    TextView Txt_production_date;
    TextView Txt_expiration_date;
    TextView Txt_shipto;
    TextView Txt_specification;
    TextView Txt_sec_specification;
    Button   Cmd_binas_clear;
    Button   Cmd_binas_submit;
    Button   Cmd_scanner;
    Button   Cmd_search;
    CheckBox Chk_invalid;
    EditText Txt_binas_barcode;
    EditText Txt_search_batch_no;
    ImageView Img_sec_scan_flag;
    TextView Txt_sec_scan_flag;

    public SearchView getTxt_search_bar() {
        return Txt_search_bar;
    }


    SearchView Txt_search_bar;
    ListView ListViewSearch;

    Timer timer, timer2;

    ListofValue Lov_Bin;
    ListofValue Lov_Reason;
    ListofValue Lov_CreatedBy;

    TabHost             hostList;
    SoapPopulateInfo    populateInfo;
    SoapPutBarcodeToBin BarcodeAssign2Bin;
    SoapPopulateBatchNo searchBatch;

    private static final int REQUEST_PERMISSION = 1;

    private static String[] PERMISSIONS_CAMERA = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    void check_camera_permission() {
        // popup permission granted
        int permission = ActivityCompat.checkSelfPermission(BinAssignActivity.this, Manifest.permission.CAMERA);
        // if dont have permit before then popup
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    BinAssignActivity.this,
                    PERMISSIONS_CAMERA,
                    REQUEST_PERMISSION
            );
        }
    }

    public void fill_info_data(CBCodeInfo obj){
        Txt_transaction_number.setText(obj.getTransaction_number());
        Txt_item_code.setText(obj.getItem_code());
        Txt_barcode_num.setText(obj.getBarcode_num());
        Txt_lot_number.setText(obj.getLot_number());
        Txt_no_koli.setText(obj.getNo_koli());
        Txt_qty.setText(obj.getQty().toString());
        Txt_uom_code.setText(obj.getUom_code());
        Txt_production_date.setText(obj.getProduction_date());
        Txt_expiration_date.setText(obj.getExpiration_date());
        Txt_specification.setText(obj.getSpecification());
        Txt_sec_specification.setText(obj.getSec_specification());
        Txt_shipto.setText(obj.getShipto());
        Txt_sec_scan_flag.setText(obj.getScan_flag());
        if(obj.getScan_flag().equals("Y")){
            Img_sec_scan_flag.setVisibility(View.VISIBLE);
        }
        else {
            Img_sec_scan_flag.setVisibility(View.GONE);
        }
        // assign lov_bin if exists
        Lov_Bin.getTxt_Lov_Text().setText(obj.getBin_checker().replace("|",""));
        Lov_Bin.getTxt_Lov_ID().setText(obj.getBin_checker().replace("|",""));

    }
    public void flushInfo(){
        Txt_transaction_number.setText("");
        Txt_item_code.setText("");
        Txt_barcode_num.setText("");
        Txt_lot_number.setText("");
        Txt_no_koli.setText("");
        Txt_qty.setText("");
        Txt_uom_code.setText("");
        Txt_production_date.setText("");
        Txt_expiration_date.setText("");
        Txt_shipto.setText("");
        Txt_specification.setText("");
        Txt_sec_specification.setText("");
        Txt_sec_scan_flag.setText("");
        Img_sec_scan_flag.setVisibility(View.GONE);

        // clear Lov Bin
        clear_Lov_Bin();
        // clear Lov Reason
        clear_Lov_Reason();
        // clear Lov CreatedBy
        clear_Lov_CreatedBy();
        // clear check in invalid if any
        Chk_invalid.setChecked(false);
    }

    public void flushImg(){
        Img_ps_1.setImageBitmap(BlankImage);
        Img_ps_2.setImageBitmap(BlankImage);
        Img_ps_3.setImageBitmap(BlankImage);
    }
    public void setImg_ps_1(byte[] data) {
        if(data.length > 0){
            Img_ps_1.setImageBitmap(BitmapFactory.decodeByteArray(data, 0, data.length));
        }
        else {
            Img_ps_1.setImageBitmap(BlankImage);
        }
    }
    public void setImg_ps_2(byte[] data) {
        if(data.length > 0){
            Img_ps_2.setImageBitmap(BitmapFactory.decodeByteArray(data, 0, data.length));
        }
        else {
            Img_ps_2.setImageBitmap(BlankImage);
        }
    }
    public void setImg_ps_3(byte[] data) {
        if(data.length > 0){
            Img_ps_3.setImageBitmap(BitmapFactory.decodeByteArray(data, 0, data.length));
        }
        else {
            Img_ps_3.setImageBitmap(BlankImage);
        }
    }

    // ======================= getter ======================================
    public ImageView getImg_ps_1() {
        return Img_ps_1;
    }
    public ImageView getImg_ps_2() {
        return Img_ps_2;
    }
    public ImageView getImg_ps_3() {
        return Img_ps_3;
    }

    public void EH_CMD_SHOW_SEARCH_BAR(Boolean status){
        if(status == true){
            Txt_search_bar.setVisibility(View.VISIBLE);
        }
        else{
            Txt_search_bar.setVisibility(View.GONE);
        }
    }

    public void EH_CMD_POPUP_DIALOG(Bitmap oBitmap, Context ctx){
        Log.d("[GudangGaram]", "EH_CMD_POPUP_DIALOG Begin");
        ihx.show_image_popup(oBitmap,ctx);
        Log.d("[GudangGaram]", "EH_CMD_POPUP_DIALOG End");
    }
    public void EH_CMD_NEXT(String strOrgId, String strBarcode){
        Toast.makeText(getApplicationContext(),strBarcode, Toast.LENGTH_LONG).show();
        EH_CMD_CLEAR();
        // call soap service for Populate info barcode
        populateInfo  = new SoapPopulateInfo(config);
        populateInfo.setContext(BinAssignActivity.this);
        populateInfo.setParentActivity(BinAssignActivity.this);
        populateInfo.Retrieve(strBarcode, strOrgId);

        // clear barcode text scanned
        Txt_binas_barcode.setText("");
    }

    public void EH_CMD_SCAN(){
        Log.d("[GudangGaram]", "BinAssignActivity :: EH_CMD_SCAN");
        IntentIntegrator SI_Scan_asset;
        Txt_binas_barcode.setFocusable(true);
        Txt_binas_barcode.requestFocus();

        try {
            SI_Scan_asset = new IntentIntegrator(BinAssignActivity.this);
            SI_Scan_asset.setCaptureActivity(CaptureCustomeActivity.class);
            SI_Scan_asset.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            SI_Scan_asset.setCameraId(0);
            SI_Scan_asset.setBeepEnabled(true);
            SI_Scan_asset.setBarcodeImageEnabled(true);
            SI_Scan_asset.initiateScan();
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "BinAssignActivity :: EH_CMD_SCAN Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void EH_CMD_CLEAR(){
        // clear info
        flushInfo();
        // clear image
        flushImg();
        // clear search
        ListViewSearch.setAdapter(null);
    }

    public void EH_CMD_SEARCH(){
        String strBatchNo;
        try{
            // call ws to retrieve data
            strBatchNo = Txt_search_batch_no.getText().toString();
            searchBatch = new SoapPopulateBatchNo(config);
            searchBatch.setContext(context);
            searchBatch.setParentActivity(BinAssignActivity.this);
            searchBatch.Retrieve(strBatchNo, mfg_org_id,  ListViewSearch);

            // clear search textbox
            Txt_search_batch_no.setText("");
        }
        catch(Exception e){
            Log.d("[GudangGaram]", " Cmd_search Exception > " + e.getMessage());
        }
    }

    public final void EH_CMD_SUBMIT(){
        String checked;
        try {
            if (Txt_barcode_num.getText().toString().length() > 0) {
                if (Chk_invalid.isChecked() == true) {
                    checked = "Y";
                } else {
                    checked = "N";
                }
                // gathered parameter field
                CBCodeBinAssign assignParam = new CBCodeBinAssign();
                assignParam.setBarcode(Txt_barcode_num.getText().toString());
                assignParam.setBin(Lov_Bin.getTxt_Lov_Text().getText().toString());
                assignParam.setOrgId(Integer.parseInt(mfg_org_id));
                assignParam.setInvalidFlag(checked);
                assignParam.setNote(Lov_Reason.getTxt_Lov_Text().getText().toString());

                // not used anymore since it already handle by login page
                //assignParam.setCreatedByNik(Lov_CreatedBy.getTxt_Lov_Text().getText().toString());
                //assignParam.setCreatedByName(Lov_CreatedBy.getTxt);
                //assignParam.setCreatedByUserId(Integer.parseInt(Lov_CreatedBy.getTxt_Lov_ID().getText().toString()));

                // using this value using from login page
                assignParam.setCreatedByUserId(Integer.parseInt(userId));


                Log.d("[GudangGaram]", " Submit Param > ");
                Log.d("[GudangGaram]", " p_barcode      : > " + Txt_barcode_num.getText().toString());
                Log.d("[GudangGaram]", " p_org_id       : > " + mfg_org_id);
                Log.d("[GudangGaram]", " p_bin_checker  : > " + Lov_Bin.getTxt_Lov_Text().getText().toString());
                Log.d("[GudangGaram]", " p_invalid_flag : > " + checked);
                Log.d("[GudangGaram]", " p_note         : > " + Lov_Reason.getTxt_Lov_Text().getText().toString());

                // call soap service for Assign Barcode To Bin
                BarcodeAssign2Bin = new SoapPutBarcodeToBin(config);
                BarcodeAssign2Bin.setContext(BinAssignActivity.this);
                BarcodeAssign2Bin.setParentActivity(BinAssignActivity.this);
                BarcodeAssign2Bin.Assign(assignParam);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", " Cmd_binas_submit Exception > " + e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed BinAssign Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("username",username);
                    upIntent.putExtra("userId", userId);
                    upIntent.putExtra("mfg_org_id",  mfg_org_id);
                    upIntent.putExtra("mfg_org_name",  mfg_org_name);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bin_assign);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        config     = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        context    = BinAssignActivity.this;
        ScanState  = config.getBoolean("ScanMode",false);

        // get passing profile from login
        username     = getIntent().getStringExtra("username");
        userId       = getIntent().getStringExtra("userId");
        mfg_org_id   = getIntent().getStringExtra("mfg_org_id");
        mfg_org_name = getIntent().getStringExtra("mfg_org_name");

        //scanneer_mode = config.getString();
        BlankImage = BitmapFactory.decodeResource(getResources(), R.drawable.noimage);

        // initialized tab page
        hostList = (TabHost)findViewById(R.id.tab_binas);
        hostList.setup();
        try{
            //Tab 1h
            TabHost.TabSpec spec = hostList.newTabSpec("Detail");
            spec.setContent(R.id.TabBodyDetail);
            spec.setIndicator("Detail");
            hostList.addTab(spec);

            //Tab 2
            spec = hostList.newTabSpec("Image");
            spec.setContent(R.id.TabBodyImage);
            spec.setIndicator("Picture");
            hostList.addTab(spec);

            //Tab 3
            spec = hostList.newTabSpec("Search");
            spec.setContent(R.id.TabBodySearch);
            spec.setIndicator("Search");
            hostList.addTab(spec);

            // focus on tab 1
            hostList.setCurrentTab(0);
        }
        catch(Exception e){
        }

        Chk_invalid                   = (CheckBox)    findViewById(R.id.chk_invalid);
        Cmd_binas_clear               = (Button)      findViewById(R.id.cmd_binas_clear);
        Cmd_binas_submit              = (Button)      findViewById(R.id.cmd_binas_submit);
        Cmd_scanner                   = (Button)      findViewById(R.id.cmd_scanner);
        Cmd_search                    = (Button)      findViewById(R.id.cmd_search);
        Txt_transaction_number        = (TextView)    findViewById(R.id.txt_transaction_number);
        Txt_item_code                 = (TextView)    findViewById(R.id.txt_item_code);
        Txt_barcode_num               = (TextView)    findViewById(R.id.txt_barcode_num);
        Txt_lot_number                = (TextView)    findViewById(R.id.txt_lot_number);
        Txt_no_koli                   = (TextView)    findViewById(R.id.txt_no_koli);
        Txt_qty                       = (TextView)    findViewById(R.id.txt_qty);
        Txt_uom_code                  = (TextView)    findViewById(R.id.txt_uom_code);
        Txt_production_date           = (TextView)    findViewById(R.id.txt_production_date);
        Txt_expiration_date           = (TextView)    findViewById(R.id.txt_expiration_date);
        Txt_shipto                    = (TextView)    findViewById(R.id.txt_shipto);
        Txt_specification             = (TextView)    findViewById(R.id.txt_specification);
        Txt_sec_specification         = (TextView)    findViewById(R.id.txt_sec_specification);
        Txt_sec_scan_flag             = (TextView)    findViewById(R.id.txt_sec_scan_flag);
        Img_sec_scan_flag             = (ImageView)   findViewById(R.id.img_sec_scan_flag);
        Img_ps_1                      = (ImageView)   findViewById(R.id.img_ps1);
        Img_ps_2                      = (ImageView)   findViewById(R.id.img_ps2);
        Img_ps_3                      = (ImageView)   findViewById(R.id.img_ps3);
        Txt_binas_barcode             = (EditText)    findViewById(R.id.txt_binas_barcode);
        Txt_search_bar                = (SearchView)  findViewById(R.id.txt_search_bar);
        Txt_search_batch_no           = (EditText)    findViewById(R.id.txt_search_batchNo);
        ListViewSearch                = (ListView)    findViewById(R.id.listViewSearch);
        Lov_Reason                    = (ListofValue) findViewById(R.id.lov_invalid_reason);
        Lov_Bin                       = (ListofValue) findViewById(R.id.lov_bin_ass);
        Lov_CreatedBy                 = (ListofValue) findViewById(R.id.lov_created_by);

        // default di hide


        if(ScanState == Boolean.TRUE){
            check_camera_permission();
            Cmd_scanner.setVisibility(View.VISIBLE);
        }
        else{
            Cmd_scanner.setVisibility(View.GONE);
        }

        Lov_Reason.setVisibility(View.GONE);
        // initalized image helper
        ihx            = new ImageHelper();
        // -------- ini combobox  -------
        init_Lov_Bin("",mfg_org_id);
        init_Lov_Reason();
        init_Lov_CreatedBy();

        // ------ init default hide search bar  --------
        EH_CMD_SHOW_SEARCH_BAR(false);

        Txt_binas_barcode.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                final String strBarcode;
                strBarcode = Txt_binas_barcode.getText().toString();
                //If the keyevent is a key-down event on the "enter" button
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    //Toast.makeText(context, strBarcode.toString(), Toast.LENGTH_LONG).show();
                    EH_CMD_NEXT(mfg_org_id, strBarcode);
                    return true;
                }
                return false;
            }
        });


        ListViewSearch.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);


        /*
        Txt_binas_barcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                final String strBarcode;
                strBarcode = s.toString();
                if(strBarcode.length() > 0) {
                    timer2 = new Timer();
                    timer2.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    EH_CMD_NEXT(mfg_org_id, strBarcode);
                                }
                            });
                        }
                    }, 600);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count > 0) {
                    // user is typing: reset already started timer (if existing)
                    if (timer2 != null)
                        timer2.cancel();
                }
            }

        });
        */

        Img_sec_scan_flag.setVisibility(View.GONE);

        Img_ps_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_1 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_ps_1.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, context);
            }
        });
        Img_ps_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_2 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_ps_2.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, context);
            }
        });
        Img_ps_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Img_3 Click");
                Bitmap bitmap = ((BitmapDrawable)Img_ps_3.getDrawable()).getBitmap();
                EH_CMD_POPUP_DIALOG(bitmap, context);
            }
        });

        Cmd_scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_SCAN();
            }
        });
        Cmd_binas_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_CLEAR();
            }
        });

        Cmd_binas_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Cmd_binas_submit " + Txt_barcode_num.getText().toString());
               EH_CMD_SUBMIT();
            }
        });

        Cmd_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", " Cmd_search ");
                EH_CMD_SEARCH();
            }
        });

        Chk_invalid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b == true){
                    // hanya clear lov bin Text saja, lov_bin id untuk restore jika di uncheck
                    Lov_Bin.getTxt_Lov_Text().setText("");
                    // refresh Lov_Bin To Disabled
                    init_Lov_Bin("",mfg_org_id);
                    // show Lov_Reason
                    Lov_Reason.setVisibility(View.VISIBLE);
                }
                else{
                    clear_Lov_Reason();
                    // recover Lov_Bin yang di clear sebelumnya
                    if(Lov_Bin.getTxt_Lov_Text().getText().toString().length() == 0 && Lov_Bin.getTxt_Lov_ID().getText().toString().length() > 0){
                        Lov_Bin.getTxt_Lov_Text().setText(Lov_Bin.getTxt_Lov_ID().getText().toString());
                        // re-enable button Lov Bin
                        Lov_Bin.getCmd_Lov().setEnabled(true);
                    }
                }

            }
        });
    }

    public void clear_Lov_Reason(){
        // clear text and id
        Lov_Reason.getTxt_Lov_Text().setText("");
        Lov_Reason.getTxt_Lov_ID().setText("");
        // hide
        Lov_Reason.setVisibility(View.GONE);
    }

    public void clear_Lov_Bin(){
        // clear text and id
        Lov_Bin.getTxt_Lov_Text().setText("");
        Lov_Bin.getTxt_Lov_ID().setText("");
        init_Lov_Bin("",mfg_org_id);
    }

    public void clear_Lov_CreatedBy(){
        Lov_CreatedBy.getTxt_Lov_Text().setText("");
        Lov_CreatedBy.getTxt_Lov_ID().setText("");
    }

    public void populate_list_search(){
    }

    public void init_Lov_Bin(String p_barcode, String p_org_id){
        if(p_barcode.length() == 0) {
            Lov_Bin.getCmd_Lov().setEnabled(false);
        }
        else {
            Lov_Bin.getCmd_Lov().setEnabled(true);

            CLovProperty propLov = new CLovProperty();
            propLov.setLovTitle("LOV Bin Persiapan");
            propLov.setLovHeaderValue("Bin Persiapan");
            propLov.setLovHeaderMeaning("Keterangan");

            List<PropertyInfo> param = new ArrayList<>();
            // =============== p_barcode ===================
            PropertyInfo prop_p_barcode = new PropertyInfo();
            prop_p_barcode.setName("p_barcode");
            prop_p_barcode.setValue(p_barcode);
            prop_p_barcode.setType(String.class);
            // =============== p_org_id ===================
            PropertyInfo prop_p_org_id = new PropertyInfo();
            prop_p_org_id.setName("p_org_id");
            prop_p_org_id.setValue(p_org_id);
            prop_p_org_id.setType(String.class);

            param.add(prop_p_barcode);
            param.add(prop_p_org_id);

            CSoapConfig soap_config = new CSoapConfig();
            soap_config.setParam_soap_namespace(config.getString("SoapNamespace", ""));
            soap_config.setParam_soap_addresss(config.getString("SoapAddress", ""));
            soap_config.setParam_soap_method("Get_List_Bin_Persiapan_checker");
            soap_config.setParam_soap_param(param);

            Lov_Bin.setProperty(propLov, this, soap_config);
        }
    }

    public void init_Lov_Reason(){
        CLovProperty propLov = new CLovProperty();
        propLov.setLovTitle("LOV Invalid Reason");
        propLov.setLovHeaderValue("Reason");
        propLov.setLovHeaderMeaning("Keterangan");

        CSoapConfig soap_config = new CSoapConfig();
        soap_config.setParam_soap_namespace(config.getString("SoapNamespace", ""));
        soap_config.setParam_soap_addresss(config.getString("SoapAddress", ""));
        soap_config.setParam_soap_method("Get_List_Reason");

        Lov_Reason.setProperty(propLov, this, soap_config);
    }


    public void init_Lov_CreatedBy(){
        CLovProperty propLov = new CLovProperty();
        propLov.setLovTitle("LOV Checker By");
        propLov.setLovHeaderValue("NIK");
        propLov.setLovHeaderMeaning("Name");

        CSoapConfig soap_config = new CSoapConfig();
        soap_config.setParam_soap_namespace(config.getString("SoapNamespace", ""));
        soap_config.setParam_soap_addresss(config.getString("SoapAddress", ""));
        soap_config.setParam_soap_method("Get_List_Employee_Checker");

        Lov_CreatedBy.setProperty(propLov, this, soap_config);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult SR_Asset;
        String StrScanAsset;

        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                }
                else
                {
                    SR_Asset = IntentIntegrator.parseActivityResult(requestCode,resultCode,intent);
                    if(SR_Asset != null){
                        try{
                            StrScanAsset = SR_Asset.getContents();
                            Txt_binas_barcode.setText(StrScanAsset);
                        }
                        catch(Exception e){
                        }
                        //EH_CMD_NEXT();
                    }
                }
                break;
            }
        }
    }

}
