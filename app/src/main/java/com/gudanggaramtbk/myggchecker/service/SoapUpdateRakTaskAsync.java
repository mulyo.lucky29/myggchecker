package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class SoapUpdateRakTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences         config;
    private Context                   context;
    private ProgressDialog            pd;
    private String                    SentResponse;
    private String                    pBatchId;
    private String                    pDeviceId;
    private String                    result;

    public SoapUpdateRakTaskAsync(){ }
    public interface SoapUpdateRakTaskAsyncResponse { void PostSentAction(String output); }
    public SoapUpdateRakTaskAsync.SoapUpdateRakTaskAsyncResponse delegate = null;
    public SoapUpdateRakTaskAsync(SoapUpdateRakTaskAsync.SoapUpdateRakTaskAsyncResponse delegate){ this.delegate = delegate; }

    public void setContext(Context context){
        this.context = context;
    }

    public void setAttribute(Context context, SharedPreferences config,String pBatchId, String pDeviceId){
        Log.d("[GudangGaram]", "SoapUpdateRakTaskAsync :: setAttribute");
        this.context          = context;
        this.config           = config;
        this.pBatchId         = pBatchId;
        this.pDeviceId        = pDeviceId;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapUpdateRakTaskAsync :: onPreExecute");
        pd.setMessage("Updating Rak Info In Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapUpdateRakTaskAsync :: onPostExecute");
        try {
            delegate.PostSentAction(output);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapUpdateRakTaskAsync :: onPostExecute Exception " + e.getMessage());
        }
        Toast.makeText(context,"Updating Rak Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d("[GudangGaram]", "SoapUpdateRakTaskAsync :: doInBackground begin");
        //Web method call
        try {
            String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
            String SOAP_ADDRESS = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
            String OPERATION_NAME = "Update_Rak";
            String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);

            SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

            // =============== p_batch_id ===================
            PropertyInfo prop_p_batch_id = new PropertyInfo();
            prop_p_batch_id.setName("p_batch_id");
            prop_p_batch_id.setValue(pBatchId);
            prop_p_batch_id.setType(String.class);
            request.addProperty(prop_p_batch_id);

            // =============== p_device_id ===================
            PropertyInfo prop_p_device_id = new PropertyInfo();
            prop_p_device_id.setName("p_device_id");
            prop_p_device_id.setValue(pDeviceId);
            prop_p_device_id.setType(String.class);
            request.addProperty(prop_p_device_id);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            //Web method call
            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                result = resultsRequestSOAP.getProperty(0).toString();
                Log.d("[GudangGaram]", "result -> " + result);
            }
            catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapUpdateRakTaskAsync :: doInBackground Exception :: Catch Http Transport : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SoapUpdateRakTaskAsync :: doInBackground end");
        }
        return "";
    }
}
