package com.gudanggaramtbk.myggchecker.model;

public class CBCodeBinAssign {
    private String Barcode;
    private Integer OrgId;
    private String Bin;
    private String InvalidFlag;
    private String Note;
    private String  CreatedByNik;
    private Integer CreatedByUserId;
    private String  CreatedByName;

    // -------- getter --------------
    public String getBarcode() {
        return Barcode;
    }
    public Integer getOrgId() {
        return OrgId;
    }
    public String getBin() {
        return Bin;
    }
    public String getInvalidFlag() {
        return InvalidFlag;
    }
    public String getNote() {
        return Note;
    }
    public String getCreatedByNik() {
        return CreatedByNik;
    }
    public Integer getCreatedByUserId() {
        return CreatedByUserId;
    }
    public String getCreatedByName() {
        return CreatedByName;
    }

    // -------- setter ---------------
    public void setBarcode(String barcode) {
        Barcode = barcode;
    }
    public void setOrgId(Integer orgId) {
        OrgId = orgId;
    }
    public void setBin(String bin) {
        Bin = bin;
    }
    public void setInvalidFlag(String invalidFlag) {
        InvalidFlag = invalidFlag;
    }
    public void setNote(String note) {
        Note = note;
    }
    public void setCreatedByNik(String createdByNik) {
        CreatedByNik = createdByNik;
    }
    public void setCreatedByUserId(Integer createdByUserId) {
        CreatedByUserId = createdByUserId;
    }
    public void setCreatedByName(String createdByName) {
        CreatedByName = createdByName;
    }

}
