package com.gudanggaramtbk.myggchecker.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.activity.BinAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBatchNoInfo;
import java.util.List;

public class SoapPopulateBatchNo {
    private SharedPreferences config;
    private Context context;
    BinAssignActivity pActivity;

    // override constructor
    public SoapPopulateBatchNo(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapPopulateBatchNo :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapPopulateBatchNo :: SetContext");
        this.context = context;
    }
    public void setParentActivity(BinAssignActivity pActivity){
        this.pActivity = pActivity;
    }

    public void Retrieve(final String pBatchNo, final String pOrgId, final  ListView pListView) {
        Log.d("[GudangGaram]", "SoapPopulateBatchNo :: Retrieve List BatchNo >> " + pBatchNo);
        try {
            // call WS
            SoapPopulateBatchNoTaskAsync SoapRequest = new SoapPopulateBatchNoTaskAsync(new SoapPopulateBatchNoTaskAsync.SoapPopulateBatchNoTaskAsyncResponse() {
                @Override
                public void PostSentAction(String result) {
                    if(result.equals("Invalid BatchNo")){
                        Toast.makeText(context, "Invalid BatchNo", Toast.LENGTH_LONG).show();
                    }
                }
            });
            SoapRequest.setAttribute(context, config,  pActivity, pOrgId, pBatchNo, pListView);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapPopulateBatchNo:: Retrieve Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {
        }
    }
}
