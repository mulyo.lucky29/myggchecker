package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.activity.LoginActivity;
import com.gudanggaramtbk.myggchecker.model.CLogin;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class SoapAuthUserTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String p_password;
    private String p_username;
    private String p_mfg_org_id;
    private String p_mfg_org_name;
    private CLogin Result;

    LoginActivity pActivity;

    public interface SoapAuthUserTaskAsyncResponse { void PostSentAction(String output); }
    public SoapAuthUserTaskAsync.SoapAuthUserTaskAsyncResponse delegate = null;
    public SoapAuthUserTaskAsync(SoapAuthUserTaskAsync.SoapAuthUserTaskAsyncResponse delegate){ this.delegate = delegate; }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setAttribute(Context context, SharedPreferences config, LoginActivity pActivity, CLogin loginInfo) {
        Log.d("[GudangGaram]", "SoapAuthUserTaskAsync :: setAttribute");
        this.context        = context;
        this.config         = config;
        this.pActivity      = pActivity;
        this.p_username     = loginInfo.getUsername();
        this.p_password     = loginInfo.getPassword();
        this.p_mfg_org_id   = loginInfo.getMfg_org_id();
        this.p_mfg_org_name = loginInfo.getMfg_org_name();

        this.pd = new ProgressDialog(this.context);

        this.Result = new CLogin();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapAuthUserTaskAsync :: onPreExecute");
        pd.setMessage("Prepare Authentication Process To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onProgressUpdate(String[] args) {
        if (pd.isShowing()) {
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapAuthUserTaskAsync :: onPostExecute");
        // message | sign valid
        if(Result.getMessage().equals("|")){
            pActivity.GoToMainPage(Result);
        }
        else{
            Toast.makeText(context, Result.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        delegate.PostSentAction(output);
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d("[GudangGaram]", "SoapAuthUserTaskAsync :: doInBackground begin");
        try {
            //Web method call
            String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
            String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
            String OPERATION_NAME = "Get_Auth_User";
            String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

            Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);
            SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

            // =============== p_username ===================
            PropertyInfo prop_p_username = new PropertyInfo();
            prop_p_username.setName("p_username");
            prop_p_username.setValue(p_username);
            prop_p_username.setType(String.class);
            request.addProperty(prop_p_username);

            // =============== p_password ===================
            PropertyInfo prop_p_password = new PropertyInfo();
            prop_p_password.setName("p_password");
            prop_p_password.setValue(p_password);
            prop_p_password.setType(String.class);
            request.addProperty(prop_p_password);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj  = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));

                /*
                  <USER_ID>-1</USER_ID>
                  <USER_NAME>|</USER_NAME>
                  <MESSAGE>Username Not Valid</MESSAGE>
                 */
                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);
                        CLogin lgn = new CLogin();
                        lgn.setUserId(obj3.getProperty(0).toString());
                        lgn.setUsername(obj3.getProperty(1).toString());
                        lgn.setMessage(obj3.getProperty(2).toString());
                        lgn.setMfg_org_id(p_mfg_org_id);
                        lgn.setMfg_org_name(p_mfg_org_name);
                        lgn.setPassword(p_password);
                        Result = lgn;
                    }
                }
                else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }

            // ----------- publish notification ----------
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // publish progress
            publishProgress("Processing Authentication");
        }
        catch(Exception e){
        }

        return "";
    }
}
