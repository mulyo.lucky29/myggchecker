package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.activity.BinAssignActivity;
import com.gudanggaramtbk.myggchecker.util.ImageHelper;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;
import static org.kobjects.base64.Base64.decode;
import android.widget.ArrayAdapter;

public class SoapRetrieveImgTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                    ResultResponse;
    private String                    SentResponse;
    private String                    barcode;
    private String                    orgId;
    private Integer                   Seq;
    private String                    message;
    BinAssignActivity                 pActivity;

    // private ArrayAdapter<DS_TaskItemDetail> trxdadapter;
    public SoapRetrieveImgTaskAsync(Context context){
        super();
        this.context = context;
    }
    public interface SoapRetrieveImgTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapRetrieveImgTaskAsync.SoapRetrieveImgTaskAsyncResponse delegate = null;
    public SoapRetrieveImgTaskAsync(SoapRetrieveImgTaskAsync.SoapRetrieveImgTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             SharedPreferences config,
                             BinAssignActivity pActivity,
                             String            pBarcode,
                             String            pOrgId){
        Log.d("[GudangGaram]", " SoapRetrieveImgTaskAsync :: setAttribute");
        this.context          = context;
        this.config           = config;
        this.pActivity        = pActivity;
        this.barcode          = pBarcode;
        this.orgId            = pOrgId;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapRetrieveImgTaskAsync :: onPreExecute :: " + message);
        pd.setMessage("Retrieve Image From Oracle ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        if(pd.isShowing()){
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
        Log.d("[GudangGaram]", " SoapRetrieveImgTaskAsync :: onProgressUpdate :: " + args[0].toString());
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        Log.d("[GudangGaram]", "SoapRetrieveImageTask :: onPostExecute >> " + ResultResponse);
        try{
            delegate.PostSentAction(ResultResponse);
        }
        catch(Exception e){
        }
        finally {
            if(output.equals("-1")){
                //Toast.makeText(context,"Retrieve Image ID = " + TaskDID + " Seq = " +  Seq + " Task Done", Toast.LENGTH_LONG).show();

            }
            else{
                //Toast.makeText(context,"Retrieve Image ID = " + TaskDID + " Seq = " +  Seq + " Task Done", Toast.LENGTH_LONG).show();
            }
            pd.dismiss();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String   v_itemCode;
        String   v_itemId;
        Integer  v_Seq;
        String   v_FA;
        String   v_FF;
        Integer  v_FS;
        String   v_Gbr;
        byte[]   vImg;
        Integer  ncount;
        Integer  nerr;
        byte[]   pCFileData;

        /*
            <ITEM_CODE>7022249</ITEM_CODE>
            <ITEM_ID>77246</ITEM_ID>
            <SEQ>1</SEQ>
            <FS_IMG>0</FS_IMG>
            <FAID_IMG>-1</FAID_IMG>
            <FFID_IMG>0</FFID_IMG>
            <FIMG/>
        */

        for(Seq=1; Seq<=3;Seq++){
            String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
            String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";

            String OPERATION_NAME = "Get_Selected_Barcode_Picture";
            String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

            SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

            // =============== p_barcode ===================
            PropertyInfo prop_p_barcode = new PropertyInfo();
            prop_p_barcode.setName("p_barcode");
            prop_p_barcode.setValue(barcode);
            prop_p_barcode.setType(String.class);

            // =============== p_org_id ===================
            PropertyInfo prop_p_org_id = new PropertyInfo();
            prop_p_org_id.setName("p_org_id");
            prop_p_org_id.setValue(orgId);
            prop_p_org_id.setType(String.class);

            // =============== p_pic_nik ===================
            PropertyInfo prop_p_seq = new PropertyInfo();
            prop_p_seq.setName("p_seq");
            prop_p_seq.setValue(Seq);
            prop_p_seq.setType(String.class);

            request.addProperty(prop_p_barcode);
            request.addProperty(prop_p_org_id);
            request.addProperty(prop_p_seq);

            Log.d("[GudangGaram]: ", "p_barcode      : " + barcode);
            Log.d("[GudangGaram]: ", "p_org_id       : " + orgId);
            Log.d("[GudangGaram]: ", "p_seq          : " + Seq);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            //Web method call
            try {
                List<HeaderProperty> headers=new ArrayList<HeaderProperty>();
                HeaderProperty headerProperty=new HeaderProperty("Accept-Encoding", "gzip,deflate");
                headers.add(headerProperty);
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS,60000);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                //httpTransport.call(SOAP_ACTION, envelope);
                httpTransport.call(SOAP_ACTION, envelope,headers);

                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();

                Log.d("[GudangGaram]", ">>> count  >>> " + ncount.toString());

                ImageHelper ihx = new ImageHelper();
                for (int i = 0; i < ncount ;i++) {
                    obj3 = (SoapObject) obj2.getProperty(i);
                    Log.d("[GudangGaram]", ">>> obj3.count[" + i + "]  >>> " + obj3.getPropertyCount());

                    v_itemCode   = obj3.getProperty(0).toString();
                    v_itemId     = obj3.getProperty(1).toString();
                    v_Seq        = Integer.parseInt(obj3.getProperty(2).toString());
                    v_FS         = Integer.parseInt(obj3.getProperty(3).toString());
                    v_FA         = obj3.getProperty(4).toString();
                    v_FF         = obj3.getProperty(5).toString();

                    Log.d("[GudangGaram]", "----------------------------------------------------------");
                    Log.d("[GudangGaram]", "[ 1 ]>>> Item Code     >>> " + v_itemCode);
                    Log.d("[GudangGaram]", "[ 2 ]>>> Item ID       >>> " + v_itemId);
                    Log.d("[GudangGaram]", "[ 3 ]>>> Seq           >>> " + v_Seq);
                    Log.d("[GudangGaram]", "[ 4 ]>>> FS            >>> " + v_FS);
                    Log.d("[GudangGaram]", "[ 5 ]>>> FAttach ID    >>> " + v_FA);
                    Log.d("[GudangGaram]", "[ 6 ]>>> FFile ID      >>> " + v_FF);

                    // image
                    try {
                        // kalo size nya ada
                        if (v_FS > 0) {
                            v_Gbr = obj3.getProperty(6).toString();
                            vImg = decode(v_Gbr);

                            if(vImg.length > ihx.getBitmapMaxSize()){
                                pCFileData = ihx.compressImageByte(vImg);
                            }
                            else{
                                pCFileData = vImg;
                            }
                            Log.d("[GudangGaram]", "[ 7 ]>>> Gbr         >>> " + v_Gbr);

                            if(Seq == 1){
                                pActivity.setImg_ps_1(pCFileData);
                            }
                            else if(Seq == 2){
                                pActivity.setImg_ps_2(pCFileData);
                            }
                            else if(Seq == 3) {
                                pActivity.setImg_ps_3(pCFileData);
                            }
                        }
                        else {
                            Log.d("[GudangGaram]", "[ 7 ]>>> Gbr Not Exist (Size = 0)");
                        }
                    }
                    catch (Exception e) {
                        Log.d("[GudangGaram]", "[ 7 ]>>> Gbr Exception >>> " + e.getMessage());
                    }
                } // end loop
            }
            catch (Exception ex) {
                Log.d("[GudangGaram]", "Catch Http Transport : " + ex.getMessage().toString());
                //Toast.makeText(context,ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
            finally {
                Log.d("[GudangGaram]", "SoapRetrieveTaskItemTaskAsync  :: end doInBackground");
            }

            publishProgress("Retrieve Image [" + Integer.toString(Seq) + " of 3]");

            try {
                Thread.sleep(500);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        } // end loop

        return "";
    }
}
