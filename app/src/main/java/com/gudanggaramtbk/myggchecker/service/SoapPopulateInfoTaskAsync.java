package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.activity.BinAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBCodeInfo;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

public class SoapPopulateInfoTaskAsync extends AsyncTask<String, String, String> {
        private SharedPreferences         config;
        private Context                   context;
        private ProgressDialog            pd;
        private String                    Response;
        private String                    pOrgid;
        private String                    pBarcode;
        private List<CBCodeInfo>          Result;
        BinAssignActivity                 pActivity;

    public SoapPopulateInfoTaskAsync(){ }
        public interface SoapPopulateInfoTaskAsyncResponse { void PostSentAction(String output); }
        public SoapPopulateInfoTaskAsync.SoapPopulateInfoTaskAsyncResponse delegate = null;
        public SoapPopulateInfoTaskAsync(SoapPopulateInfoTaskAsync.SoapPopulateInfoTaskAsyncResponse delegate){ this.delegate = delegate; }

        public void setContext(Context context){
            this.context = context;
        }

        public void setAttribute(Context context, SharedPreferences config, BinAssignActivity pActivity, String pOrgid, String pBarcode){
            Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: setAttribute");
            this.context          = context;
            this.config           = config;
            this.pBarcode         = pBarcode;
            this.pOrgid           = pOrgid;
            this.pActivity        = pActivity;
            this.pd = new ProgressDialog(this.context);
            Result = new ArrayList<CBCodeInfo>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: onPreExecute");
            pd.setMessage("Retrieve Info Item From Oracle ... ");
            pd.setIndeterminate(false);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected void onPostExecute(String output) {
            super.onPostExecute(output);
            pd.dismiss();
            Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: onPostExecute > " + output);
            try {
                if (Result.size() == 0) {
                    Toast.makeText(context, "No Data Result", Toast.LENGTH_LONG).show();
                }
                else{
                    // fill info data
                    pActivity.fill_info_data(Result.get(0));
                    // re-render lov bin
                    pActivity.init_Lov_Bin(pBarcode, pOrgid);
                    // call ws to load image
                    SoapRetrieveImgTaskAsync soapRequest = new SoapRetrieveImgTaskAsync(new SoapRetrieveImgTaskAsync.SoapRetrieveImgTaskAsyncResponse() {
                        @Override
                        public void PostSentAction(String output) {
                        }
                    });
                    soapRequest.setAttribute(context, config, pActivity, pBarcode,pOrgid);
                    if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                        //work on sgs3 android 4.0.4
                        soapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                    }
                    else {
                        soapRequest.execute(); // work on sgs2 android 2.3
                    }
                }
                delegate.PostSentAction(output);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: onPostExecute Exception " + e.getMessage());
            }
            Toast.makeText(context,Response, Toast.LENGTH_LONG).show();
        }

        @Override
        protected String doInBackground(String... params) {
             /*
                  <Table diffgr:id="Table1" msdata:rowOrder="0">
                      0. <TRANSACTION_NUMBER>4585/PNJ-MS/11/20/GSI</TRANSACTION_NUMBER>
                      1. <ITEM_CODE>2032738</ITEM_CODE>
                      2. <BARCODE_NUMBER>000423965</BARCODE_NUMBER>
                      3. <LOT_NUMBER>000270603</LOT_NUMBER>
                      4. <NO_KOLI>0001</NO_KOLI>
                      5. <QUANTITY>1</QUANTITY>
                      6. <UOM_CODE>PC</UOM_CODE>
                      7. <PRODUCTION_DATE>2020-10-13T00:00:00+07:00</PRODUCTION_DATE>
                      8. <EXPIRATION_DATE>2025-10-13T00:00:00+07:00</EXPIRATION_DATE>
                      9. <PICKING_LIST_HEADER_ID>55765</PICKING_LIST_HEADER_ID>
                      10. <PICKING_LIST_LINE_ID>596095</PICKING_LIST_LINE_ID>
                      11. <ITEM_ID>750848</ITEM_ID>
                      12. <ITEM_SPECIFICATION>PART NO. : 0AE3815 SUB. ASSEMBLY : TAV-08.21AE48.01.0009 SEARCH TEXT : 0212010203 APPLICATION : CELLOPHANER C800 </ITEM_SPECIFICATION>
                      13. <PICKER_ID>424015</PICKER_ID>
                      14. <TRX_SOURCE_NAME>NPB</TRX_SOURCE_NAME>
                      15. <ITEM_SEC_SPECIFICATION>-</ITEM_SEC_SPECIFICATION>
                      16. <SHIP_TO>Loading Dock Selatan Ph3</SHIP_TO>
                      17. <BIN_CHECKER>99-07</BIN_CHECKER>
                      18. <SCAN_FLAG>N</SCAN_FLAG>
                   </Table>
             */

            Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: doInBackground begin");
            //Web method call
            try {
                String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
                String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
                String OPERATION_NAME = "Get_Selected_Barcode_Info";
                String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";
                Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);

                SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
                Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

                // =============== p_org_id ===================
                PropertyInfo prop_p_org_id = new PropertyInfo();
                prop_p_org_id.setName("p_org_id");
                prop_p_org_id.setValue(pOrgid);
                prop_p_org_id.setType(String.class);
                request.addProperty(prop_p_org_id);

                // =============== p_barcode ===================
                PropertyInfo prop_p_barcode = new PropertyInfo();
                prop_p_barcode.setName("p_barcode");
                prop_p_barcode.setValue(pBarcode);
                prop_p_barcode.setType(String.class);
                request.addProperty(prop_p_barcode);

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                try {
                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                    httpTransport.debug = true;
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    httpTransport.call(SOAP_ACTION, envelope);
                    SoapObject obj, obj1, obj2, obj3;

                    obj  = (SoapObject) envelope.getResponse();
                    obj1 = (SoapObject) obj.getProperty("diffgram");
                    obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                        Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));
                        Result.clear();
                        if (obj2.getPropertyCount() > 0) {
                            for (int i = 0; i < obj2.getPropertyCount(); i++) {
                                obj3 = (SoapObject) obj2.getProperty(i);
                                CBCodeInfo dii = new CBCodeInfo();
                                dii.setTransaction_number(obj3.getProperty(0).toString());
                                dii.setItem_code(obj3.getProperty(1).toString());
                                dii.setBarcode_num(obj3.getProperty(2).toString());
                                dii.setLot_number(obj3.getProperty(3).toString());
                                dii.setNo_koli(obj3.getProperty(4).toString());
                                dii.setQty(Double.parseDouble(obj3.getProperty(5).toString()));
                                dii.setUom_code(obj3.getProperty(6).toString());
                                dii.setProduction_date(obj3.getProperty(7).toString());
                                dii.setExpiration_date(obj3.getProperty(8).toString());
                                dii.setPicking_list_header_id(obj3.getProperty(9).toString());
                                dii.setPicking_list_line_id(obj3.getProperty(10).toString());
                                dii.setItem_id(obj3.getProperty(11).toString());
                                dii.setSpecification(obj3.getProperty(12).toString().replace("|",""));
                                dii.setPicker_id(obj3.getProperty(13).toString());
                                dii.setTrx_source_name(obj3.getProperty(14).toString());
                                dii.setSec_specification(obj3.getProperty(15).toString().replace("|",""));
                                dii.setShipto(obj3.getProperty(16).toString().replace("|",""));
                                dii.setBin_checker(obj3.getProperty(17).toString());
                                dii.setScan_flag(obj3.getProperty(18).toString());

                                Result.add(dii);

                                if(obj3.getProperty(18).toString().equals("Y")){
                                    Response = "Barcode Already Scanned";
                                }
                                else{
                                    Response = "Retrieve Data Done";
                                }

                            }
                        }
                        else{
                            Response = "Invalid Barcode";
                        }
                }
                catch(Exception e){
                    Log.d("[GudangGaram]", "Catch Exception " + e.getMessage().toString());
                    // kalo nga nemu barcode nya
                    Response = "Invalid Barcode";
                }
            }
            catch (Exception ex) {
                Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: doInBackground Exception :: Catch Http Transport : " + ex.getMessage().toString());
            }
            finally {
                Log.d("[GudangGaram]", "SoapPopulateInfoTaskAsync :: doInBackground end");
            }
            return Response;
        }
    }
