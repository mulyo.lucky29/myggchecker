package com.gudanggaramtbk.myggchecker.model;

public class CBCodeInfo {
    private String transaction_number;
    private String item_code;
    private String barcode_num;
    private String lot_number;
    private String no_koli;
    private Double qty;
    private String uom_code;
    private String production_date;
    private String expiration_date;
    private String picking_list_header_id;
    private String picking_list_line_id;
    private String item_id;
    private String specification;
    private String picker_id;
    private String trx_source_name;
    private String sec_specification;
    private String shipto;
    private String bin_checker;
    private String scan_flag;

    // ---------- getter --------------
    public String getTransaction_number() {  return transaction_number; }
    public String getItem_code()          {  return item_code; }
    public String getBarcode_num()        {  return barcode_num; }
    public String getLot_number()         {  return lot_number;  }
    public String getNo_koli()            {  return no_koli;  }
    public String getUom_code()           {  return uom_code; }
    public String getProduction_date()    {  return production_date; }
    public String getExpiration_date()    {  return expiration_date; }
    public String getShipto()             {  return shipto;  }
    public String getSpecification()      {  return specification;  }
    public String getSec_specification()  {  return sec_specification;  }
    public Double getQty()                {  return qty;  }
    public String getPicking_list_header_id() {
        return picking_list_header_id;
    }
    public String getPicking_list_line_id() {
        return picking_list_line_id;
    }
    public String getItem_id() {
        return item_id;
    }
    public String getPicker_id() {
        return picker_id;
    }
    public String getTrx_source_name() {
        return trx_source_name;
    }
    public String getBin_checker() {  return bin_checker;  }
    public String getScan_flag() {  return scan_flag; }

    // ---------- setter --------------
    public void setTransaction_number(String transaction_number) {
        this.transaction_number = transaction_number;
    }
    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }
    public void setBarcode_num(String barcode_num) {
        this.barcode_num = barcode_num;
    }
    public void setLot_number(String lot_number) {
        this.lot_number = lot_number;
    }
    public void setNo_koli(String no_koli) {
        this.no_koli = no_koli;
    }
    public void setUom_code(String uom_code) {
        this.uom_code = uom_code;
    }
    public void setProduction_date(String production_date) {
        this.production_date = production_date;
    }
    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }
    public void setShipto(String shipto) {
        this.shipto = shipto;
    }
    public void setSpecification(String specification) {
        this.specification = specification;
    }
    public void setSec_specification(String sec_specification) {
        this.sec_specification = sec_specification;
    }
    public void setQty(Double qty) {
        this.qty = qty;
    }
    public void setPicking_list_line_id(String picking_list_line_id) {
        this.picking_list_line_id = picking_list_line_id;
    }
    public void setPicking_list_header_id(String picking_list_header_id) {
        this.picking_list_header_id = picking_list_header_id;
    }
    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }
    public void setPicker_id(String picker_id) {
        this.picker_id = picker_id;
    }
    public void setTrx_source_name(String trx_source_name) {
        this.trx_source_name = trx_source_name;
    }
    public void setBin_checker(String bin_checker) {
        this.bin_checker = bin_checker;
    }
    public void setScan_flag(String scan_flag) {
        this.scan_flag = scan_flag;
    }

}
