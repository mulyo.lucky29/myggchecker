package com.gudanggaramtbk.myggchecker.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import com.google.android.material.navigation.NavigationView;
import com.gudanggaramtbk.myggchecker.R;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private SharedPreferences config;
    private String            DeviceID;
    private String            username;
    private String            userId;
    private String            mfg_org_id;
    private String            mfg_org_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //}
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        config         = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        // get information intent
        DeviceID       = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        //String sysdate = new Date().toString();
        //Log.d("[GudangGaram]", "sysdate >> " + sysdate);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        TextView nav_device_id = (TextView) hView.findViewById(R.id.txt_xdevice_id);
        TextView nav_username  = (TextView) hView.findViewById(R.id.txt_xusername);

        TextView Txt_mfg_org_name = findViewById(R.id.txt_mfg_org_name);
        TextView Txt_mfg_org_id   = findViewById(R.id.txt_mfg_org_id);
        Button Cmd_Assign_bin     = findViewById(R.id.cmd_assign_bin);
        Button Cmd_Assign_rak     = findViewById(R.id.cmd_assign_rak);

        // get passing profile from login
        username     = getIntent().getStringExtra("username");
        userId       = getIntent().getStringExtra("userId");
        mfg_org_id   = getIntent().getStringExtra("mfg_org_id");
        mfg_org_name = getIntent().getStringExtra("mfg_org_name");

        Txt_mfg_org_id.setText(mfg_org_id);
        Txt_mfg_org_name.setText(mfg_org_name);

        nav_device_id.setText(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
        nav_username.setText(username);

        //navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        // ------------- responsibility restriction ---------
        Menu menu = navigationView.getMenu();
        if(username.equals("default")){
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_bin_assign).setVisible(false);
            menu.findItem(R.id.nav_rak_assign).setVisible(false);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);

            Cmd_Assign_bin.setVisibility(View.GONE);
            Cmd_Assign_rak.setVisibility(View.GONE);
        }
        else{
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_bin_assign).setVisible(true);
            menu.findItem(R.id.nav_rak_assign).setVisible(true);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);

            Cmd_Assign_bin.setVisibility(View.VISIBLE);
            Cmd_Assign_rak.setVisibility(View.VISIBLE);
        }

        Cmd_Assign_bin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_bin_assign();
            }
        });

        Cmd_Assign_rak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_rak_assign();
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            EH_cmd_about();
        }
        else if (id == R.id.nav_setting) {
            EH_cmd_setting();
        }
        else if (id == R.id.nav_bin_assign) {
            EH_cmd_bin_assign();
        }
        else if (id == R.id.nav_rak_assign) {
            EH_cmd_rak_assign();
        }
        else if (id == R.id.nav_exit) {
            EH_cmd_exit();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void EH_cmd_bin_assign(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_bin_assign");
        Intent I_Bin_Assign;
        I_Bin_Assign = new Intent(this, BinAssignActivity.class);
        I_Bin_Assign.putExtra("username",username);
        I_Bin_Assign.putExtra("userId", userId);
        I_Bin_Assign.putExtra("mfg_org_id",  mfg_org_id);
        I_Bin_Assign.putExtra("mfg_org_name",  mfg_org_name);
        startActivity(I_Bin_Assign);
    }

    public void EH_cmd_rak_assign(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_rak_assign");
        Intent I_Rak_Assign;
        I_Rak_Assign = new Intent(this, RakAssignActivity.class);
        I_Rak_Assign.putExtra("username",username);
        I_Rak_Assign.putExtra("userId", userId);
        I_Rak_Assign.putExtra("mfg_org_id",  mfg_org_id);
        I_Rak_Assign.putExtra("mfg_org_name",  mfg_org_name);
        startActivity(I_Rak_Assign);
    }

    public void EH_cmd_setting(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_setting");
        Intent I_Setting;
        I_Setting = new Intent(this, SettingActivity.class);
        I_Setting.putExtra("username",username);
        I_Setting.putExtra("userId", userId);
        I_Setting.putExtra("mfg_org_id",  mfg_org_id);
        I_Setting.putExtra("mfg_org_name",  mfg_org_name);
        startActivity(I_Setting);
    }

    public void EH_cmd_about(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_about");
        Intent I_About;
        I_About = new Intent(this, AboutActivity.class);
        I_About.putExtra("username",username);
        I_About.putExtra("userId", userId);
        I_About.putExtra("mfg_org_id",  mfg_org_id);
        I_About.putExtra("mfg_org_name",  mfg_org_name);
        startActivity(I_About);
    }

    public void EH_cmd_exit(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_exit");
        Intent objsignOut = new Intent(getBaseContext(),LoginActivity.class);
        objsignOut.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(objsignOut);
    }

}
