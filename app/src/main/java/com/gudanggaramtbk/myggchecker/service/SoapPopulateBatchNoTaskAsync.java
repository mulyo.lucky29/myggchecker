package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.activity.BinAssignActivity;
import com.gudanggaramtbk.myggchecker.adapter.CustomListAdapterBatchNo;
import com.gudanggaramtbk.myggchecker.model.CBatchNoInfo;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

public class SoapPopulateBatchNoTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                     Response;
    private String                     pOrgid;
    private String                     pBatchNo;
    private List<CBatchNoInfo>         Result;
    private ListView                   populateSearchList;
    private ArrayAdapter<CBatchNoInfo> searchAdapter;

    BinAssignActivity pActivity;

    public SoapPopulateBatchNoTaskAsync(){ }
    public interface SoapPopulateBatchNoTaskAsyncResponse { void PostSentAction(String output); }
    public SoapPopulateBatchNoTaskAsync.SoapPopulateBatchNoTaskAsyncResponse delegate = null;
    public SoapPopulateBatchNoTaskAsync(SoapPopulateBatchNoTaskAsync.SoapPopulateBatchNoTaskAsyncResponse delegate){ this.delegate = delegate; }

    public void setContext(Context context){
        this.context = context;
    }

    public void setAttribute(Context context, SharedPreferences config, BinAssignActivity pActivity, String pOrgid, String pBatchNo, ListView oListView){
        Log.d("[GudangGaram]", "SoapPopulateBatchNoTaskAsync :: setAttribute");
        this.context            = context;
        this.config             = config;
        this.pBatchNo           = pBatchNo;
        this.pOrgid             = pOrgid;
        this.pActivity          = pActivity;
        this.populateSearchList = oListView;
        this.pd = new ProgressDialog(this.context);
        Result = new ArrayList<CBatchNoInfo>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapPopulateBatchNoTaskAsync :: onPreExecute");

        pd.setMessage("Retrieve List From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapPopulateBatchNoTaskAsync :: onPostExecute > " + output);
        try {
            try {
                populateSearchList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                searchAdapter = new CustomListAdapterBatchNo(context, Result, populateSearchList);
                searchAdapter.notifyDataSetChanged();
                populateSearchList.setAdapter(searchAdapter);
                long xcount = populateSearchList.getCount();
                Log.d("[GudangGaram]:", "SoapPopulateBatchNoTaskAsync OnPostExecute xcount " + xcount);
                if (xcount == 0) {
                    Toast.makeText(context, "No Data Result", Toast.LENGTH_LONG).show();
                }
                /*
                // keep hide no need to view
                else{
                    pActivity.EH_CMD_SHOW_SEARCH_BAR(true);
                    // search filter
                    pActivity.getTxt_search_bar().setQueryHint("Search Here..");
                    pActivity.getTxt_search_bar().setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String txt) { return false; }
                        @Override
                        public boolean onQueryTextChange(String txt) {
                            Log.d("[GudangGaram]", "Txt_search_bar :: onQueryTextChange : " + txt);
                            searchAdapter.getFilter().filter(txt);
                            searchAdapter.notifyDataSetChanged();
                            return true;
                        }
                    });
                }
                */
            }
            catch(Exception e){
                Log.d("[GudangGaram]:", "SoapPopulateBatchNoTaskAsync OnPostExecute Exception " + e.getMessage());
            }
            finally {
                delegate.PostSentAction(output);
            }
            /*
            if (Result.size() == 0) {
                Toast.makeText(context, "No Data Result", Toast.LENGTH_LONG).show();
            }
            else{
            }
            */
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapPopulateBatchNoTaskAsync :: onPostExecute Exception " + e.getMessage());
        }
        Toast.makeText(context,Response, Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d("[GudangGaram]", "SoapPopulateBatchNoTaskAsync :: doInBackground begin");
        //Web method call
        try {
            String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
            String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
            String OPERATION_NAME = "Get_Search_Barcode_Batch";

            String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);
            SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

            // =============== p_org_id ===================
            PropertyInfo prop_p_org_id = new PropertyInfo();
            prop_p_org_id.setName("p_org_id");
            prop_p_org_id.setValue(pOrgid);
            prop_p_org_id.setType(String.class);
            request.addProperty(prop_p_org_id);

            // =============== p_batch_no ===================
            PropertyInfo prop_p_batch_no = new PropertyInfo();
            prop_p_batch_no.setName("p_batch_no");
            prop_p_batch_no.setValue(pBatchNo);
            prop_p_batch_no.setType(String.class);
            request.addProperty(prop_p_batch_no);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj  = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));
                Result.clear();
                /*
                    <Table diffgr:id="Table1" msdata:rowOrder="0">
                    <BATCH_NUMBER>51512</BATCH_NUMBER>
                    <ITEM_ID>645416</ITEM_ID>
                    <ITEM_CODE>9002455</ITEM_CODE>
                    <ITEM_DESC>MOUNTING RAIL</ITEM_DESC>
                    <ORGANIZATION_ID>368</ORGANIZATION_ID>
                    <BARCODE_NUMBER>000382509</BARCODE_NUMBER>
                    <RAK_SEMENTARA>P-4-1-R</RAK_SEMENTARA>
                    <BIN_SEMENTARA>00-00</BIN_SEMENTARA>
                    </Table>
                 */

                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);
                        CBatchNoInfo dii = new CBatchNoInfo();
                        dii.setBatch_no(obj3.getProperty(0).toString());
                        dii.setItem_id(obj3.getProperty(1).toString());
                        dii.setItem_code(obj3.getProperty(2).toString());
                        dii.setItem_desc(obj3.getProperty(3).toString());
                        dii.setOrg_id(Integer.parseInt(obj3.getProperty(4).toString()));
                        dii.setBarcode_num(obj3.getProperty(5).toString());
                        dii.setRak_sementara(obj3.getProperty(6).toString());
                        dii.setBin_sementara(obj3.getProperty(7).toString());
                        Result.add(dii);
                    }
                }
                else{
                    Response = "Invalid BatchNum";
                }
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "Catch Exception " + e.getMessage().toString());
                // kalo nga nemu barcode nya
                Response = "Invalid BatchNum";
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapPopulateBatchNoTaskAsync :: doInBackground Exception :: Catch Http Transport : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SoapPopulateBatchNoTaskAsync :: doInBackground end");
        }
        return Response;
    }
}
