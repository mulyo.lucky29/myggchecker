package com.gudanggaramtbk.myggchecker.service;

public interface DownloadTaskAsyncResponse {
    void PostDownloadAction(String output);
}
