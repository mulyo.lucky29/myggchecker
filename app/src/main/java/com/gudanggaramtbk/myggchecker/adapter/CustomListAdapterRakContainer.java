package com.gudanggaramtbk.myggchecker.adapter;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.widget.CompoundButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import com.gudanggaramtbk.myggchecker.R;
import com.gudanggaramtbk.myggchecker.model.CBinInfo;
import com.gudanggaramtbk.myggchecker.viewholder.ViewHolderBinInfo;
import java.util.ArrayList;
import java.util.List;

public class CustomListAdapterRakContainer extends ArrayAdapter<CBinInfo> {
    Context            mcontext;
    List<CBinInfo>     scanList;
    ArrayList<String>  stringArrayList;
    ArrayList<Boolean> positionArray;
    ListView           lvi;
    private SparseBooleanArray mSelectedItemsIds;
    private LayoutInflater mInflater;

    public CustomListAdapterRakContainer(Context context, List<CBinInfo> list, ListView objlvi)
    {
        super(context,0,list);
        mcontext     = context;
        scanList     = list;
        mInflater    = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lvi          = objlvi;
        mSelectedItemsIds = new  SparseBooleanArray();
    }

    @Override
    public int getCount(){
        return scanList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void AllChecked(Boolean isChecked){
        for( CBinInfo element : scanList ) {
            Log.d("[GudangGaram]:", "4.AllChecked -> " + element.getBin_checker().toString());
            element.setChecked(isChecked);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderBinInfo holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlist_rak_assign, parent, false);
            holder = new ViewHolderBinInfo();
            holder.chk                   = (CheckBox) convertView.findViewById(R.id.chk_rwl);
            holder.Xtxt_checkerid        = (TextView) convertView.findViewById(R.id.txt_rwl_checkerid);
            holder.Xtxt_bin_persiapan    = (TextView) convertView.findViewById(R.id.txt_rwl_bin);
            holder.Xtxt_sourcename       = (TextView) convertView.findViewById(R.id.txt_rwl_sourcename);
            holder.Xtxt_source_header_id = (TextView) convertView.findViewById(R.id.txt_rwl_source_header_id);
            holder.Xtxt_trx_no           = (TextView) convertView.findViewById(R.id.txt_rwl_trx_no);
            holder.Xtxt_count            = (TextView) convertView.findViewById(R.id.txt_rwl_count);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderBinInfo) convertView.getTag();
            holder.chk.setOnClickListener(null);
        }

        holder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("[GudangGaram]:", "chk >> " +  holder.chk.isChecked() + " - " + holder.Xtxt_bin_persiapan.getText().toString() + " - " + holder.Xtxt_trx_no.getText().toString() + " - " + holder.Xtxt_count.getText().toString());
                scanList.get(position).setChecked(isChecked);
                getItem(position).setChecked(isChecked);
                lvi.setItemChecked(position,getItem(position).getChecked());
                notifyDataSetChanged();
            }
        });

        // fill info in view holder
        holder.chk.setChecked(getItem(position).getChecked());
        holder.Xtxt_checkerid.setText(getItem(position).getChecker_id().toString());
        holder.Xtxt_bin_persiapan.setText(getItem(position).getBin_checker().toString());
        holder.Xtxt_sourcename.setText(getItem(position).getTrx_source_name().toString());
        holder.Xtxt_source_header_id.setText(getItem(position).getTrx_source_header_id().toString());
        holder.Xtxt_trx_no.setText(getItem(position).getTrx_number().toString());
        holder.Xtxt_count.setText(getItem(position).getNitem().toString());

        return convertView;
    }
}
