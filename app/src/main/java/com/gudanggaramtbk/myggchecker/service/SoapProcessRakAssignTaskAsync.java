package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.activity.RakAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBinInfo;
import com.gudanggaramtbk.myggchecker.util.Utilities;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SoapProcessRakAssignTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences         config;
    private Context                   context;
    private ProgressDialog            pd;
    private String                    SentResponse;
    private List<CBinInfo>            plist;
    private String                    prak;
    private String                    pbatch;
    private String                    pdeviceid;
    private String                    result;
    RakAssignActivity                 pActivity;
    Integer count;
    Integer total;
    String salt;

    public SoapProcessRakAssignTaskAsync (){ }
    public interface SoapProcessRakAssignTaskAsyncResponse { void PostSentAction(String output); }
    public SoapProcessRakAssignTaskAsync.SoapProcessRakAssignTaskAsyncResponse delegate = null;
    public SoapProcessRakAssignTaskAsync(SoapProcessRakAssignTaskAsync.SoapProcessRakAssignTaskAsyncResponse delegate){ this.delegate = delegate; }

    public void setContext(Context context){
        this.context = context;
    }

    public void setAttribute(Context context, SharedPreferences config, RakAssignActivity pActivity, List<CBinInfo> plist, String prak, String pdeviceid){
        Log.d("[GudangGaram]", "SoapProcessRakAssignTaskAsync :: setAttribute");
        this.context          = context;
        this.config           = config;
        this.prak             = prak;
        this.plist            = plist;
        this.pdeviceid        = pdeviceid;
        this.pActivity        = pActivity;
        this.pd = new ProgressDialog(this.context);

        Utilities u = new Utilities();
        //String sysdate = u.replace_format_indo(new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date()));\
        String sysdate = new Date().toString();
        salt     = pdeviceid + "-" + prak + "-" + sysdate;
        pbatch   = Utilities.md5(salt);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapProcessRakAssignTaskAsync :: onPreExecute");
        pd.setMessage("Prepare To Send To Interface Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        if (pd.isShowing()) {
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapProcessRakAssignTaskAsync :: onPostExecute");
        try{
            if(count == total){
                // call ws to process update rak
                SoapUpdateRak soapRequest = new SoapUpdateRak(config);
                soapRequest.setContext(context);
                soapRequest.Update(pbatch,pdeviceid);
            }
            // cleanup lov Rak Selected
            pActivity.clear_Lov_Rak_Persiapan();
            // cleanup Rak Container List
            pActivity.clear_rak_container();
        }
        catch(Exception e){
        }
        delegate.PostSentAction(output);
        Toast.makeText(context,"Process Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d("[GudangGaram]", "SoapProcessRakAssignTaskAsync :: doInBackground begin");
        Log.d("[GudangGaram]", "SoapProcessRakAssignTaskAsync :: count list > " + plist.size());

        count = 0;
        total = plist.size();

        //Web method call
        for( CBinInfo elbin : plist) {
            try {
                String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
                String SOAP_ADDRESS   = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
                String OPERATION_NAME = "Put_Bin_To_Rak";
                String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

                Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);
                SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
                Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

                // =============== p_rak ===================
                PropertyInfo prop_p_rak = new PropertyInfo();
                prop_p_rak.setName("p_rak");
                prop_p_rak.setValue(prak);
                prop_p_rak.setType(String.class);
                request.addProperty(prop_p_rak);

                // =============== p_device_id ===================
                PropertyInfo prop_p_device_id = new PropertyInfo();
                prop_p_device_id.setName("p_device_id");
                prop_p_device_id.setValue(pdeviceid);
                prop_p_device_id.setType(String.class);
                request.addProperty(prop_p_device_id);

                // =============== p_batch_id ===================
                PropertyInfo prop_p_batch_id = new PropertyInfo();
                prop_p_batch_id.setName("p_batch_id");
                prop_p_batch_id.setValue(pbatch);
                prop_p_batch_id.setType(String.class);
                request.addProperty(prop_p_batch_id);

                // =============== p_checker_id ===================
                PropertyInfo prop_p_checker_id = new PropertyInfo();
                prop_p_checker_id.setName("p_checker_id");
                prop_p_checker_id.setValue(elbin.getChecker_id());
                prop_p_checker_id.setType(String.class);
                request.addProperty(prop_p_checker_id);

                // =============== p_bin_checker ===================
                PropertyInfo prop_p_bin_checker = new PropertyInfo();
                prop_p_bin_checker.setName("p_bin_checker");
                prop_p_bin_checker.setValue(elbin.getBin_checker());
                prop_p_bin_checker.setType(String.class);
                request.addProperty(prop_p_bin_checker);

                // =============== p_trx_source_name ===================
                PropertyInfo prop_p_trx_source_name = new PropertyInfo();
                prop_p_trx_source_name.setName("p_trx_source_name");
                prop_p_trx_source_name.setValue(elbin.getTrx_source_name());
                prop_p_trx_source_name.setType(String.class);
                request.addProperty(prop_p_trx_source_name);

                // =============== p_trx_source_header_id ===================
                PropertyInfo prop_p_trx_source_header_id = new PropertyInfo();
                prop_p_trx_source_header_id.setName("p_trx_source_header_id");
                prop_p_trx_source_header_id.setValue(elbin.getTrx_source_header_id());
                prop_p_trx_source_header_id.setType(Long.class);
                request.addProperty(prop_p_trx_source_header_id);

                // =============== p_trx_number ===================
                PropertyInfo prop_p_trx_number = new PropertyInfo();
                prop_p_trx_number.setName("p_trx_number");
                prop_p_trx_number.setValue(elbin.getTrx_number());
                prop_p_trx_number.setType(String.class);
                request.addProperty(prop_p_trx_number);

                // =============== p_nitem ===================
                PropertyInfo prop_p_nitem = new PropertyInfo();
                prop_p_nitem.setName("p_nitem");
                prop_p_nitem.setValue(elbin.getNitem());
                prop_p_nitem.setType(Integer.class);
                request.addProperty(prop_p_nitem);

                Log.d("[GudangGaram]", "p_rak                  :" + prak);
                Log.d("[GudangGaram]", "p_device_id            :" + pdeviceid);
                Log.d("[GudangGaram]", "p_batch_id             :" + pbatch);
                Log.d("[GudangGaram]", "p_checker_id           :" + elbin.getChecker_id());
                Log.d("[GudangGaram]", "p_bin_checker          :" + elbin.getBin_checker());
                Log.d("[GudangGaram]", "p_trx_source_name      :" + elbin.getTrx_source_name());
                Log.d("[GudangGaram]", "p_trx_source_header_id :" + elbin.getTrx_source_header_id());
                Log.d("[GudangGaram]", "p_trx_number           :" + elbin.getTrx_number());
                Log.d("[GudangGaram]", "p_nitem                :" + elbin.getNitem());

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                //Web method call
                try {
                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                    httpTransport.debug = true;
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    httpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                    result = resultsRequestSOAP.getProperty(0).toString();
                    Log.d("[GudangGaram]", "result -> " + result);
                }
                catch (NullPointerException e) {
                    Log.d("[GudangGaram]", "Catch NullPointerException");
                }
            }
            catch (Exception ex) {
                Log.d("[GudangGaram]", "SoapProcessRakAssignTaskAsync :: doInBackground Exception :: Catch Http Transport : " + ex.getMessage().toString());
            }
            finally {
                Log.d("[GudangGaram]", "SoapProcessRakAssignTaskAsync :: doInBackground end");
            }

            // ----------- publish notification ----------
            try {
                Thread.sleep(20);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            // publish progress
            publishProgress("Process Bin "  + elbin.getBin_checker().toString());
            count +=1;
        } // end for

        // call another WS to process

        return "";
    }
}
