package com.gudanggaramtbk.myggchecker.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;
import com.gudanggaramtbk.myggchecker.R;
import com.gudanggaramtbk.myggchecker.model.CBatchNoInfo;
import com.gudanggaramtbk.myggchecker.viewholder.ViewHolderBatchInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CustomListAdapterBatchNo extends ArrayAdapter<CBatchNoInfo> implements Filterable {
    Context mcontext;
    List<CBatchNoInfo> TrxList;
    ListView olisttask;
    ArrayList<Boolean> positionArray;

    private List<CBatchNoInfo> originalData = null;
    private List<CBatchNoInfo> filteredData = null;

    private LayoutInflater mInflater;
    public boolean checkBoxState[];

    public CustomListAdapterBatchNo (Context context, List<CBatchNoInfo> list, ListView listTask)
    {
        super(context,0,list);
        mcontext = context;
        TrxList = list;
        checkBoxState=new boolean[list.size()];
        olisttask = listTask;

        filteredData  = list;
        originalData  = list;

        mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }

    public CBatchNoInfo getItemAtPosition(int position){
        return filteredData.get(position);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolderBatchInfo holder;
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rowlist_search_batch, parent, false);
            holder = new ViewHolderBatchInfo();

            holder.Xtxt_batchNo            = (TextView) convertView.findViewById(R.id.txt_src_batchNo);
            holder.Xtxt_itemId             = (TextView) convertView.findViewById(R.id.txt_src_itemId);
            holder.Xtxt_itemCode           = (TextView) convertView.findViewById(R.id.txt_src_itemCode);
            holder.Xtxt_itemDesc           = (TextView) convertView.findViewById(R.id.txt_src_itemDesc);
            holder.Xtxt_barcodeNo          = (TextView) convertView.findViewById(R.id.txt_src_barcodeNo);
            holder.Xtxt_rakSementara       = (TextView) convertView.findViewById(R.id.txt_src_rakSementara);
            holder.Xtxt_binSementara       = (TextView) convertView.findViewById(R.id.txt_src_binSementara);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderBatchInfo) convertView.getTag();
        }

        TextView Xtxt_batchNo          =  holder.Xtxt_batchNo;
        TextView Xtxt_itemId           =  holder.Xtxt_itemId;
        TextView Xtxt_itemCode         =  holder.Xtxt_itemCode;
        TextView Xtxt_itemDesc         =  holder.Xtxt_itemDesc;
        TextView Xtxt_barcodeNo        =  holder.Xtxt_barcodeNo;
        TextView Xtxt_rakSementara     =  holder.Xtxt_rakSementara;
        TextView Xtxt_binSementara     =  holder.Xtxt_binSementara;

        CBatchNoInfo o = getItem(position);
        Xtxt_batchNo.setText(o.getBatch_no());
        Xtxt_itemId.setText(o.getItem_id());
        Xtxt_itemCode.setText(o.getItem_code());
        Xtxt_itemDesc.setText(o.getItem_desc());
        Xtxt_barcodeNo.setText(o.getBarcode_num());
        Xtxt_rakSementara.setText(o.getRak_sementara());
        Xtxt_binSementara.setText(o.getBin_sementara());

        return convertView;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charText) {
                Log.d("[GudangGaram]", "CustomListAdapterBatchNo :: getFilter :: performFiltering");

                String FindWord = charText.toString().toLowerCase(Locale.getDefault());
                String filterableString;

                FilterResults filterResults = new FilterResults();
                final List<CBatchNoInfo> list = originalData;
                int count = list.size();
                final ArrayList<CBatchNoInfo> nlist = new ArrayList<CBatchNoInfo>(count);

                for(int i = 0; i< count; i++){
                    filterableString = list.get(i).getBarcode_num() + list.get(i).getItem_code() + list.get(i).getItem_desc() + list.get(i).getRak_sementara() + list.get(i).getBin_sementara();
                    if(filterableString.toLowerCase().contains(FindWord)){
                        nlist.add(list.get(i));
                    }
                }
                filterResults.values = nlist;
                filterResults.count  = nlist.size();

                return filterResults;
            };

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence charText, FilterResults results) {
                Log.d("[GudangGaram]", "CustomListAdapterBatchNo :: publishResults charText :: " + charText.toString());
                filteredData = (ArrayList<CBatchNoInfo>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
