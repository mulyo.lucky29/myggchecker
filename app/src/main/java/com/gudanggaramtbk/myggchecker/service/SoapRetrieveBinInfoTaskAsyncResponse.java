package com.gudanggaramtbk.myggchecker.service;

public interface SoapRetrieveBinInfoTaskAsyncResponse {
    void PostSentAction(String output);
}
