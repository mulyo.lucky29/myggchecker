package com.gudanggaramtbk.myggchecker.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.R;
import com.gudanggaramtbk.myggchecker.model.CLogin;
import com.gudanggaramtbk.myggchecker.service.SoapAuthUser;

public class LoginActivity extends AppCompatActivity {
    private SharedPreferences config;
    private Context context;
    boolean doubleBackToExitPressedOnce         = false;

    Button Cmd_Login;
    EditText Txt_Username;
    EditText Txt_Password;
    CLogin loginInfo;

    @Override
    public void onBackPressed() {
        try{
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
             Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
        catch(Exception e){
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        config     = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        context    = LoginActivity.this;

        Cmd_Login                     = (Button)      findViewById(R.id.cmd_login);
        Txt_Username                  = (EditText)    findViewById(R.id.txt_username);
        Txt_Password                  = (EditText)    findViewById(R.id.txt_password);

        Cmd_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EH_CMD_LOGIN();
            }
        });
        loginInfo = new CLogin();

        Txt_Password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Txt_Password.setTransformationMethod(new PasswordTransformationMethod());
                Txt_Password.setTransformationMethod(null);

            }
        });

        /*
        textViewShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textViewShowPassword.getText().toString().equalsIgnoreCase("Show")) {
                    editTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    textViewShowPassword.setText("Hide");
                    editTextPassword.setSelection(editTextPassword.getText().length());
                } else {
                    textViewShowPassword.setText("Show");
                    editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
                    editTextPassword.setSelection(editTextPassword.getText().length());
                }

            }
        });
        */

    }

    public boolean validate_input(){
        Log.d("[GudangGaram]", "LoginActivity :: validate_input");
        Boolean result = false;
        try{
                // -------- validate username ----------
                if(Txt_Username.getText().toString().trim().equals("")){
                    Txt_Username.setError("Username should be filled");
                    result = false;
                }
                else{
                    Txt_Username.setError(null);
                    result = true;
                }
                // -------- validate password ----------
                if(Txt_Password.getText().toString().trim().equals("")){
                    Txt_Password.setError("Password should be filled");
                    result = false;
                }
                else{
                    Txt_Password.setError(null);
                    result = true;
                }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: validate_input Exception > " + e.getMessage());
        }
        return result;
    }

    public void EH_CMD_LOGIN(){
        Log.d("[GudangGaram]", "LoginActivity :: EH_CMD_LOGIN");
        String v_txt_password = "";
        String v_txt_username = "";

        v_txt_username     = Txt_Username.getText().toString();
        v_txt_password     = Txt_Password.getText().toString();

        Log.d("[GudangGaram]", "LoginActivity :: EH_CMD_LOGIN > username > " + v_txt_username);
        Log.d("[GudangGaram]", "LoginActivity :: EH_CMD_LOGIN > password > " + v_txt_password);

        try{
            // set parameter login info
            loginInfo.setUsername(v_txt_username);
            loginInfo.setPassword(v_txt_password);
            loginInfo.setMfg_org_id("368");
            loginInfo.setMfg_org_name("GMSPI - Gudang Induk Sparepart Gempol");

            if(validate_input()){
                if(v_txt_username.equals("default")){
                    GoToMainPage(loginInfo);
                }
                else{
                    Log.d("[GudangGaram]", "LoginActivity :: EH_CMD_LOGIN > check to web service");
                    // call WS Auth to validate login to EBS
                    SoapAuthUser auth = new SoapAuthUser(config);
                    auth.setContext(context);
                    auth.setParentActivity(LoginActivity.this);
                    auth.Validate(loginInfo);
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: EH_CMD_LOGIN Exception > " + e.getMessage());
        }
    }

    public void GoToMainPage(CLogin loginInfo){
        try{
            Intent I_Main;
            I_Main = new Intent(this, MainActivity.class);
            I_Main.putExtra("username",  loginInfo.getUsername());
            I_Main.putExtra("userId",    loginInfo.getUserId());
            I_Main.putExtra("mfg_org_id", loginInfo.getMfg_org_id() );
            I_Main.putExtra("mfg_org_name",  loginInfo.getMfg_org_name());
            startActivity(I_Main);
        }
        catch (Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: GoToMainPage Exception > " + e.getMessage());
        }
    }
}
