package com.gudanggaramtbk.myggchecker.viewholder;

import android.widget.CheckBox;
import android.widget.TextView;

public class ViewHolderBinInfo {
    public CheckBox chk;
    public TextView Xtxt_checkerid;
    public TextView Xtxt_bin_persiapan;
    public TextView Xtxt_sourcename;
    public TextView Xtxt_source_header_id;
    public TextView Xtxt_trx_no;
    public TextView Xtxt_count;
}
