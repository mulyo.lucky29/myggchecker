package com.gudanggaramtbk.myggchecker.model;

public class CBinInfo {
    private Boolean checked;
    private String checker_id;
    private String bin_checker;
    private String trx_source_name;
    private String trx_source_header_id;
    private String trx_number;
    private String nitem;


    // ---------- getter ---------------
    public Boolean getChecked() { return checked; }
    public String getChecker_id() {
        return checker_id;
    }
    public String getBin_checker() {
        return bin_checker;
    }
    public String getTrx_source_name() {
        return trx_source_name;
    }
    public String getTrx_source_header_id() {
        return trx_source_header_id;
    }
    public String getTrx_number() {
        return trx_number;
    }
    public String getNitem() {
        return nitem;
    }

    // ---------- setter ---------------
    public void setChecked(Boolean checked) {  this.checked = checked; }
    public void setChecker_id(String checker_id) {
        this.checker_id = checker_id;
    }
    public void setBin_checker(String bin_checker) {
        this.bin_checker = bin_checker;
    }
    public void setTrx_source_name(String trx_source_name) {  this.trx_source_name = trx_source_name;  }
    public void setTrx_source_header_id(String trx_source_header_id) {  this.trx_source_header_id = trx_source_header_id;  }
    public void setTrx_number(String trx_number) {
        this.trx_number = trx_number;
    }
    public void setNitem(String nitem) {
        this.nitem = nitem;
    }
}
