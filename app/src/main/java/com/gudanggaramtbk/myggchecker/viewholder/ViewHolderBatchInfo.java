package com.gudanggaramtbk.myggchecker.viewholder;

import android.widget.TextView;

public class ViewHolderBatchInfo {
    public TextView Xtxt_batchNo;
    public TextView Xtxt_itemId;
    public TextView Xtxt_itemCode;
    public TextView Xtxt_itemDesc;
    public TextView Xtxt_barcodeNo;
    public TextView Xtxt_rakSementara;
    public TextView Xtxt_binSementara;
}
