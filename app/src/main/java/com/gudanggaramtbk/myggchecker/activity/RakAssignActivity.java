package com.gudanggaramtbk.myggchecker.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;
import com.gudanggaramtbk.listofvalue.ListofValue;
import com.gudanggaramtbk.listofvalue.model.CLovProperty;
import com.gudanggaramtbk.listofvalue.model.CSoapConfig;
import com.gudanggaramtbk.myggchecker.R;
import com.gudanggaramtbk.myggchecker.adapter.CustomListAdapterRakContainer;
import com.gudanggaramtbk.myggchecker.model.CBinInfo;
import com.gudanggaramtbk.myggchecker.service.SoapProcessRakAssign;
import com.gudanggaramtbk.myggchecker.service.SoapRetrieveBinInfo;
import java.util.ArrayList;
import java.util.HashMap;

public class RakAssignActivity extends AppCompatActivity {
    private SharedPreferences config;
    private Context context;

    ListView                    listRakContainer;
    ListofValue                 Lov_Bin;
    ListofValue                 Lov_Rak;
    CheckBox                    ChkTitle;
    Button                      Cmd_Delete;
    Button                      Cmd_Add;
    Button                      Cmd_Done;
    private ArrayList<CBinInfo>  arrayList;
    CustomListAdapterRakContainer adapter;
    private String username;
    private String userId;
    private String mfg_org_id;
    private String mfg_org_name;

    HashMap<String, CBinInfo> rak_container;
    String deviceId;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed RakAssign Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("username",username);
                    upIntent.putExtra("userId", userId);
                    upIntent.putExtra("mfg_org_id",  mfg_org_id);
                    upIntent.putExtra("mfg_org_name",  mfg_org_name);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rak_assign);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        config           = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        context          = RakAssignActivity.this;

        // get passing profile from login
        username     = getIntent().getStringExtra("username");
        userId       = getIntent().getStringExtra("userId");
        mfg_org_id   = getIntent().getStringExtra("mfg_org_id");
        mfg_org_name = getIntent().getStringExtra("mfg_org_name");

        deviceId         = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        Lov_Bin          = (ListofValue) findViewById(R.id.lov_bin_rakas);
        Lov_Rak          = (ListofValue) findViewById(R.id.lov_rak_rakas);
        ChkTitle         = (CheckBox)    findViewById(R.id.chk_tbl_chk);
        Cmd_Delete       = (Button)      findViewById(R.id.cmd_rakass_delete);
        Cmd_Add          = (Button)      findViewById(R.id.cmd_rakass_add);
        Cmd_Done         = (Button)      findViewById(R.id.cmd_rakas_done);
        listRakContainer = (ListView)    findViewById(R.id.list_rak_container);

        rak_container    = new HashMap<String, CBinInfo>();
        arrayList        = new ArrayList<CBinInfo>();
        listRakContainer.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listRakContainer.setClickable(true);

        adapter = new CustomListAdapterRakContainer(context, arrayList, listRakContainer);
        adapter.notifyDataSetChanged();
        listRakContainer.setAdapter(adapter);

        // -------- init lov -------------
        init_Lov_Bin_Persiapan();
        init_Lov_Rak_Persiapan();

        ChkTitle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                try{
                    EH_CMD_Select_All(isChecked);
                }
                catch(Exception e){
                }
            }
        });

        Cmd_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    EH_CMD_Delete();
                }
                catch(Exception e){
                }
            }
        });

        Cmd_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String bin_persiapan;
                    bin_persiapan = Lov_Bin.getTxt_Lov_Text().getText().toString();
                    EH_CMD_Add(bin_persiapan);
                }
                catch(Exception e){
                }
            }
        });

        Cmd_Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    EH_CMD_Done();
                }
                catch (Exception e){
                }
            }
        });
    }

    public void reapply_distinct_filter(){
        // clear filter first
        rak_container.clear();
        Log.d("[GudangGaram]", "arraylist count > " + arrayList.size());
        // re-apply the filter
        for(CBinInfo obj : arrayList){
            if(!rak_container.containsKey(obj.getBin_checker())){
                Log.d("[GudangGaram]", "Apply To Hash > " + obj.getBin_checker().toString());
                rak_container.put(obj.getBin_checker(), obj);
            }
        }
    }

    public void add_bin2rak(CBinInfo obj){
        Log.d("[GudangGaram]", "before arraylist count > " + adapter.getCount());
            if(!rak_container.containsKey(obj.getBin_checker())){
                // add hash
                rak_container.put(obj.getBin_checker(), obj);
                // add adapter
                adapter.add(obj);
                adapter.notifyDataSetChanged();
            }
            else{
                Toast.makeText(context, "Bin "  + obj.getBin_checker().toString() + " Already Add", Toast.LENGTH_LONG).show();
            }
        Log.d("[GudangGaram]", "after arraylist count > " + arrayList.size());
    }

    public void delete_bin_from_rak(Integer idx){
        Log.d("[GudangGaram]", "before delete_rak_container> " + rak_container.size());
        // remove hash
        rak_container.remove(adapter.getItem(idx).getBin_checker());
        Log.d("[GudangGaram]", "after delete_rak_container > " + rak_container.size());
        Log.d("[GudangGaram]", "delete_bin_from_rak > " + adapter.getItem(idx).getBin_checker().toString());
        // remove adapter list
        adapter.remove(adapter.getItem(idx));
    }

    public void clear_Lov_Bin_Persiapan(){
        Lov_Bin.getTxt_Lov_Text().setText("");
        Lov_Bin.getTxt_Lov_ID().setText("");
    }

    public void clear_Lov_Rak_Persiapan(){
        Lov_Rak.getTxt_Lov_Text().setText("");
        Lov_Rak.getTxt_Lov_ID().setText("");
    }

    public void clear_rak_container(){
        try{
            // clear adapter
            adapter.clear();
            // clear hash
            rak_container.clear();
        }
        catch(Exception e){
        }
    }

    public void EH_CMD_Add(String strBin){
        Log.d("[GudangGaram]", "rak_container > " + rak_container.size());
        if(strBin.length() > 0){
            // call ws to get result info and add to list
            SoapRetrieveBinInfo ws = new SoapRetrieveBinInfo(config);
            ws.setContext(context);
            ws.setParentActivity(RakAssignActivity.this);
            ws.Retrieve(strBin,mfg_org_id);
        }
        else{
            Toast.makeText(context, "No Bin Add", Toast.LENGTH_LONG).show();
        }
    }

    public void EH_CMD_Delete(){
        CustomListAdapterRakContainer adptr = (CustomListAdapterRakContainer) listRakContainer.getAdapter();
        SparseBooleanArray checkeds = listRakContainer.getCheckedItemPositions();

        Log.d("[GudangGaram]:", "adapter  count   >> " + adapter.getCount());
        Log.d("[GudangGaram]:", "hashlist count   >> " + rak_container.size());

        if(adptr.isEmpty()) {
            Toast.makeText(context, "Rak Container Is Empty", Toast.LENGTH_LONG).show();
        }
        else{
            // check yang di pilih ada  nga ?
            if(checkeds.size() > 0){
                // use it to remind the next one don't forget subtract 1 in list.
                int delCounter = 0;
                for(int i = 0; i < checkeds.size(); i++)
                {
                    // indexing sparse item
                    int key = checkeds.keyAt(i);
                    if(checkeds.get(key))
                    {
                        adptr.remove(adptr.getItem(key - delCounter));
                        delCounter += 1;
                    }
                }
                adptr.notifyDataSetChanged();
                listRakContainer.clearChoices();
                listRakContainer.requestLayout();
                // re-apply distinct filter
                reapply_distinct_filter();
            }
            else{
                Toast.makeText(context, "None Selected To Delete", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void EH_CMD_Select_All(Boolean isChecked){
        if(adapter.isEmpty()){
            Toast.makeText(context, "Rak Container Already Empty", Toast.LENGTH_LONG).show();
        }
        else {
            adapter.AllChecked(isChecked);
            adapter.notifyDataSetChanged();
        }
    }

    public void EH_CMD_Done(){
        int nerr = 0;
        // check container rak
        if(adapter.isEmpty()){
            Toast.makeText(context, "No Bin Assigned In Rak Container", Toast.LENGTH_LONG).show();
            nerr = nerr + 1;
        }
        // check Lov_Rak Assigned
        if(Lov_Rak.getTxt_Lov_Text().length() == 0){
            Toast.makeText(context, "No Rak Selected", Toast.LENGTH_LONG).show();
            nerr = nerr + 1;
        }

        if(nerr == 0){
            // call ws to process rak assign
            SoapProcessRakAssign ws = new SoapProcessRakAssign(config);
            ws.setContext(context);
            ws.setParentActivity(RakAssignActivity.this);
            ws.Send(arrayList, Lov_Rak.getTxt_Lov_Text().getText().toString(), deviceId);
        }
    }

    public void init_Lov_Bin_Persiapan(){
            CLovProperty propLov    = new CLovProperty();
            propLov.setLovTitle("LOV Bin Persiapan");
            propLov.setLovHeaderValue("Bin Persiapan");
            propLov.setLovHeaderMeaning("Keterangan");

            CSoapConfig soap_config = new CSoapConfig();
            soap_config.setParam_soap_namespace(config.getString("SoapNamespace", ""));
            soap_config.setParam_soap_addresss(config.getString("SoapAddress", ""));
            soap_config.setParam_soap_method("Get_List_Bin_Persiapan_Ass");

            Lov_Bin.setProperty(propLov, this, soap_config);
    }

    public void init_Lov_Rak_Persiapan(){
            CLovProperty propLov    = new CLovProperty();
            propLov.setLovTitle("LOV Rak Persiapan");
            propLov.setLovHeaderValue("Rak Persiapan");
            propLov.setLovHeaderMeaning("Keterangan");

            CSoapConfig soap_config = new CSoapConfig();
            soap_config.setParam_soap_namespace(config.getString("SoapNamespace", ""));
            soap_config.setParam_soap_addresss(config.getString("SoapAddress", ""));
            soap_config.setParam_soap_method("Get_List_Rak_Persiapan_Ass");

            Lov_Rak.setProperty(propLov, this, soap_config);
    }

}