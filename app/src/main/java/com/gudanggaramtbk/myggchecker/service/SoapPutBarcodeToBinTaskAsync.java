package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.activity.BinAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBCodeBinAssign;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class SoapPutBarcodeToBinTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private String                    SentResponse;
    private String                    result;
    BinAssignActivity                 pActivity;
    CBCodeBinAssign                   assignParam;

    public SoapPutBarcodeToBinTaskAsync (){ }
    public interface SoapPutBarcodeToBinTaskAsyncResponse { void PostSentAction(String output); }
    public SoapPutBarcodeToBinTaskAsync.SoapPutBarcodeToBinTaskAsyncResponse delegate = null;
    public SoapPutBarcodeToBinTaskAsync(SoapPutBarcodeToBinTaskAsync.SoapPutBarcodeToBinTaskAsyncResponse delegate){ this.delegate = delegate; }

    public void setContext(Context context){
        this.context = context;
    }

    public void setAttribute(Context context, SharedPreferences config, BinAssignActivity pActivity, CBCodeBinAssign assignParam){
        Log.d("[GudangGaram]", "SoapPutBarcodeToBinTaskAsync :: setAttribute");
        this.context          = context;
        this.config           = config;
        this.pActivity        = pActivity;
        this.assignParam      = assignParam;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapPutBarcodeToBinTaskAsync :: onPreExecute");
        pd.setMessage("Prepare To Assign Barcode To Bin ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        if (pd.isShowing()) {
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapPutBarcodeToBinTaskAsync :: onPostExecute");
        try{
            // clear all inputs
            pActivity.EH_CMD_CLEAR();
        }
        catch(Exception e){
        }
        delegate.PostSentAction(output);
        Toast.makeText(context,"Process Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d("[GudangGaram]", "SoapPutBarcodeToBinTaskAsync :: doInBackground begin");

        //Web method call
            try {
                String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
                String SOAP_ADDRESS   = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
                String OPERATION_NAME = "Put_Barcode_Item_To_Bin_N";
                String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

                Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);
                SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
                Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

                // =============== p_barcode ===================
                PropertyInfo prop_p_barcode = new PropertyInfo();
                prop_p_barcode.setName("p_barcode");
                prop_p_barcode.setValue(assignParam.getBarcode());
                prop_p_barcode.setType(String.class);
                request.addProperty(prop_p_barcode);

                // =============== p_org_id ===================
                PropertyInfo prop_p_org_id = new PropertyInfo();
                prop_p_org_id.setName("p_org_id");
                prop_p_org_id.setValue(assignParam.getOrgId());
                prop_p_org_id.setType(String.class);
                request.addProperty(prop_p_org_id);

                // =============== p_bin_checker ===================
                PropertyInfo prop_p_bin_checker = new PropertyInfo();
                prop_p_bin_checker.setName("p_bin_checker");
                prop_p_bin_checker.setValue(assignParam.getBin());
                prop_p_bin_checker.setType(String.class);
                request.addProperty(prop_p_bin_checker);

                // =============== p_invalid_flag ===================
                PropertyInfo prop_p_invalid_flag = new PropertyInfo();
                prop_p_invalid_flag.setName("p_invalid_flag");
                prop_p_invalid_flag.setValue(assignParam.getInvalidFlag());
                prop_p_invalid_flag.setType(String.class);
                request.addProperty(prop_p_invalid_flag);

                // =============== p_note ===================
                PropertyInfo prop_p_note = new PropertyInfo();
                prop_p_note.setName("p_note");
                prop_p_note.setValue(assignParam.getNote());
                prop_p_note.setType(String.class);
                request.addProperty(prop_p_note);

                // =============== p_note ===================
                PropertyInfo prop_p_created_by = new PropertyInfo();
                prop_p_created_by.setName("p_created_by");
                prop_p_created_by.setValue(assignParam.getCreatedByUserId());
                prop_p_created_by.setType(Number.class);
                request.addProperty(prop_p_created_by);


                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                //Web method call
                try {
                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                    httpTransport.debug = true;
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    httpTransport.call(SOAP_ACTION, envelope);
                    SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                    result = resultsRequestSOAP.getProperty(0).toString();
                    Log.d("[GudangGaram]", "result -> " + result);
                }
                catch (NullPointerException e) {
                    Log.d("[GudangGaram]", "Catch NullPointerException");
                }

                // ----------- publish notification ----------
                try {
                    Thread.sleep(2000);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            catch (Exception ex) {
                Log.d("[GudangGaram]", "SoapPutBarcodeToBinTaskAsync :: doInBackground Exception :: Catch Http Transport : " + ex.getMessage().toString());
            }
            finally {
                Log.d("[GudangGaram]", "SoapPutBarcodeToBinTaskAsync :: doInBackground end");
            }


        return "";
    }
}
