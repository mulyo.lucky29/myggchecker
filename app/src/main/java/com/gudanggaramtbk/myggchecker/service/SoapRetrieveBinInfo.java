package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.gudanggaramtbk.myggchecker.activity.RakAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBinInfo;
import java.util.ArrayList;
import java.util.List;

public class SoapRetrieveBinInfo{
        private SharedPreferences config;
        private Context context;
        RakAssignActivity pActivity;
        List<CBinInfo> TResult;

        // override constructor
        public SoapRetrieveBinInfo(SharedPreferences PConfig){
            Log.d("[GudangGaram]", "SoapRetrieveBinInfo :: Constructor");
            this.config = PConfig;
        }
        public void setContext(Context context){
            Log.d("[GudangGaram]", "SoapRetrieveBinInfo :: SetContext");
            this.context = context;
        }
        public void setParentActivity(RakAssignActivity pActivity){
            this.pActivity = pActivity;
        }

        public void Retrieve(final String pBin, final String pOrgId) {
            Log.d("[GudangGaram]", "SoapRetrieveBinInfo :: Retrieve Bin Info >> " + pBin);
            try {
                // call WS
                SoapRetrieveBinInfoTaskAsync SoapRequest = new SoapRetrieveBinInfoTaskAsync(new SoapRetrieveBinInfoTaskAsync.SoapRetrieveBinInfoTaskAsyncResponse() {
                    @Override
                    public void PostSentAction(String RTaskID) {
                        Log.d("[GudangGaram]", "SoapRetrieveBinInfo :: Retrieve Bin Info >> ");
                    }
                });
                SoapRequest.setAttribute(context, config,  pActivity, pOrgId, pBin);
                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                    //work on sgs3 android 4.0.4
                    SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                }
                else {
                    SoapRequest.execute(); // work on sgs2 android 2.3
                }
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "SoapRetrieveBinInfo :: Retrieve Exception : " + e.getMessage().toString());
                e.printStackTrace();
            }
            finally {
            }
        }
}