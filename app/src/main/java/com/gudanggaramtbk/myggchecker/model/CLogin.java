package com.gudanggaramtbk.myggchecker.model;

public class CLogin {
    private String username;
    private String password;
    private String message;
    private String userId;
    private String mfg_org_id;
    private String mfg_org_name;

    // --------- getter --------------
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public String getMessage() {
        return message;
    }
    public String getUserId() {
        return userId;
    }
    public String getMfg_org_id() {
        return mfg_org_id;
    }
    public String getMfg_org_name() {
        return mfg_org_name;
    }

    // ------------ setter -------------
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public void setMfg_org_id(String mfg_org_id) {
        this.mfg_org_id = mfg_org_id;
    }
    public void setMfg_org_name(String mfg_org_name) {
        this.mfg_org_name = mfg_org_name;
    }



}
