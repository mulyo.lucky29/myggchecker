package com.gudanggaramtbk.myggchecker.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.gudanggaramtbk.myggchecker.activity.RakAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBinInfo;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

/*
   <Table diffgr:id="Table1" msdata:rowOrder="0">
   <CHECKER_ID>416821</CHECKER_ID>
   <BIN_CHECKER>99-07</BIN_CHECKER>
   <TRX_SOURCE_NAME>NPB</TRX_SOURCE_NAME>
   <TRX_SOURCE_HEADER_ID>129997</TRX_SOURCE_HEADER_ID>
   <TRX_NUMBER>PNJ/GMP/000003099</TRX_NUMBER>
   <NITEM>2</NITEM>
   </Table>
*/

public class SoapRetrieveBinInfoTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences         config;
    private Context                   context;
    private ProgressDialog            pd;
    private String                    SentResponse;
    private String                    pOrgid;
    private String                    pBin;
    private List<CBinInfo>            Result;
    RakAssignActivity                 pActivity;

    public SoapRetrieveBinInfoTaskAsync(){ }

    public interface SoapRetrieveBinInfoTaskAsyncResponse { void PostSentAction(String output); }
    public SoapRetrieveBinInfoTaskAsync.SoapRetrieveBinInfoTaskAsyncResponse delegate = null;
    public SoapRetrieveBinInfoTaskAsync(SoapRetrieveBinInfoTaskAsync.SoapRetrieveBinInfoTaskAsyncResponse delegate){ this.delegate = delegate; }

    public void setContext(Context context){
        this.context = context;
    }

    public void setAttribute(Context context, SharedPreferences config, RakAssignActivity pActivity, String pOrgid, String pBin){
        Log.d("[GudangGaram]", "SoapRetrieveBinInfoTaskAsync:: setAttribute");
        this.context          = context;
        this.config           = config;
        this.pBin             = pBin;
        this.pOrgid           = pOrgid;
        this.pActivity        = pActivity;
        this.pd = new ProgressDialog(this.context);
        Result = new ArrayList<CBinInfo>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("[GudangGaram]", "SoapRetrieveBinInfoTaskAsync :: onPreExecute");
        pd.setMessage("Retrieve Bin Info From Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapRetrieveBinInfoTaskAsync :: onPostExecute");
        try {
            if (Result.size() == 0) {
                Toast.makeText(context, "No Bin Data Result", Toast.LENGTH_LONG).show();
            }
            else{
                // add to list
                pActivity.add_bin2rak(Result.get(0));
                // clear lov bin
                pActivity.clear_Lov_Bin_Persiapan();
            }
            delegate.PostSentAction(output);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapRetrieveBinInfoTaskAsync :: onPostExecute Exception " + e.getMessage());
        }
        Toast.makeText(context,"Retrieve Bin Data Done", Toast.LENGTH_LONG).show();
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d("[GudangGaram]", "SoapRetrieveBinInfoTaskAsync :: doInBackground begin");
        //Web method call
        try {
            String NAMESPACE    = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
            String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
            String OPERATION_NAME = "Get_Selected_Bin_Info";
            String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + SOAP_ACTION);
            SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
            Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

            // =============== p_org_id ===================
            PropertyInfo prop_p_org_id = new PropertyInfo();
            prop_p_org_id.setName("p_org_id");
            prop_p_org_id.setValue(pOrgid);
            prop_p_org_id.setType(String.class);
            request.addProperty(prop_p_org_id);

            // =============== p_bin_checker ===================
            PropertyInfo prop_p_bin_checker = new PropertyInfo();
            prop_p_bin_checker.setName("p_bin_checker");
            prop_p_bin_checker.setValue(pBin);
            prop_p_bin_checker.setType(String.class);
            request.addProperty(prop_p_bin_checker);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);

            try {
                HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;

                obj  = (SoapObject) envelope.getResponse();
                obj1 = (SoapObject) obj.getProperty("diffgram");
                obj2 = (SoapObject) obj1.getProperty("NewDataSet");

                Log.d("[GudangGaram]", ">>> count  >>> " + Integer.toString(obj2.getPropertyCount()));
                Result.clear();

                if (obj2.getPropertyCount() > 0) {
                    for (int i = 0; i < obj2.getPropertyCount(); i++) {
                        obj3 = (SoapObject) obj2.getProperty(i);
                        CBinInfo bii = new CBinInfo();
                        bii.setChecked(false); // default false
                        bii.setChecker_id(obj3.getProperty(0).toString());
                        bii.setBin_checker(obj3.getProperty(1).toString());
                        bii.setTrx_source_name(obj3.getProperty(2).toString());
                        bii.setTrx_source_header_id(obj3.getProperty(3).toString());
                        bii.setTrx_number(obj3.getProperty(4).toString());
                        bii.setNitem(obj3.getProperty(5).toString());

                        Log.d("[GudangGaram]", "obj3.getProperty(0) >>" + obj3.getProperty(0).toString());
                        Result.add(bii);
                    }
                }
                else {
                    Toast.makeText(context, "No Data Found", Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException e) {
                Log.d("[GudangGaram]", "Catch NullPointerException");
            }
        }
        catch (Exception ex) {
            Log.d("[GudangGaram]", "SoapRetrieveBinInfoTaskAsync :: doInBackground Exception :: Catch Http Transport : " + ex.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "SoapRetrieveBinInfoTaskAsync :: doInBackground end");
        }
        return "";
    }
}
