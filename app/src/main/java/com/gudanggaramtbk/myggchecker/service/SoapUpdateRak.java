package com.gudanggaramtbk.myggchecker.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

public class SoapUpdateRak {
    private SharedPreferences config;
    private Context context;

    // override constructor
    public SoapUpdateRak(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapUpdateRak :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapUpdateRak :: SetContext");
        this.context = context;
    }
    public void Update(final String pBatchId, final String pDeviceId) {
        Log.d("[GudangGaram]", "SoapUpdateRak :: Process Batch_Id >> " + pBatchId);
        try {
            // call WS to update rack
            SoapUpdateRakTaskAsync SoapRequest = new SoapUpdateRakTaskAsync(new SoapUpdateRakTaskAsync.SoapUpdateRakTaskAsyncResponse() {
                @Override
                public void PostSentAction(String RTaskID) {
                }
            });
            SoapRequest.setAttribute(context, config,  pBatchId, pDeviceId);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapUpdateRak :: Retrieve Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
    }
}
