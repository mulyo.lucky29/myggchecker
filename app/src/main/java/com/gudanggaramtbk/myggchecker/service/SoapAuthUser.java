package com.gudanggaramtbk.myggchecker.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.gudanggaramtbk.myggchecker.activity.LoginActivity;
import com.gudanggaramtbk.myggchecker.model.CLogin;

public class SoapAuthUser {
    private SharedPreferences config;
    private Context context;
    LoginActivity pActivity;

    // override constructor
    public SoapAuthUser(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapAuthUser :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapAuthUser :: SetContext");
        this.context = context;
    }
    public void setParentActivity(LoginActivity pActivity){
        this.pActivity = pActivity;
    }
    public void Validate(final CLogin loginInfo) {
        Log.d("[GudangGaram]", "SoapAuthUser :: Validate User >> " + loginInfo.getUsername());
        try {
            // call WS
            SoapAuthUserTaskAsync SoapRequest = new SoapAuthUserTaskAsync(new SoapAuthUserTaskAsync.SoapAuthUserTaskAsyncResponse() {
                @Override
                public void PostSentAction(String RTaskID) {
                    Log.d("[GudangGaram]", "SoapAuthUser :: PostSentAction");
                }
            });
            SoapRequest.setAttribute(context, config,  pActivity, loginInfo);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapAuthUser :: Validate Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {
        }
    }
}
