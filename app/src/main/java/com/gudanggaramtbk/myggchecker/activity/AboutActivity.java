package com.gudanggaramtbk.myggchecker.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.gudanggaramtbk.myggchecker.R;
import com.gudanggaramtbk.myggchecker.util.DownloadUpdate;

public class AboutActivity extends AppCompatActivity {
    private DownloadUpdate           updaterApk;
    private SharedPreferences        config;
    private Button                   CmdUpdateApk;

    private String username;
    private String userId;
    private String mfg_org_id;
    private String mfg_org_name;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed About Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("username",username);
                    upIntent.putExtra("userId", userId);
                    upIntent.putExtra("mfg_org_id",  mfg_org_id);
                    upIntent.putExtra("mfg_org_name",  mfg_org_name);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // get passing profile from main page
        username     = getIntent().getStringExtra("username");
        userId       = getIntent().getStringExtra("userId");
        mfg_org_id   = getIntent().getStringExtra("mfg_org_id");
        mfg_org_name = getIntent().getStringExtra("mfg_org_name");

        // initialize updater apk
        updaterApk = new DownloadUpdate(config);
        updaterApk.setContext(this);

        CmdUpdateApk = (Button)findViewById(R.id.cmd_updateAPK);
        CmdUpdateApk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updaterApk.DownloadApk();
            }
        });

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }
}
