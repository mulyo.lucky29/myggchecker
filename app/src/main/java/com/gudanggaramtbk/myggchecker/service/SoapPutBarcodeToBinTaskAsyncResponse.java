package com.gudanggaramtbk.myggchecker.service;

public interface SoapPutBarcodeToBinTaskAsyncResponse {
    void PostSentAction(String output);
}
