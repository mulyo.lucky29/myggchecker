package com.gudanggaramtbk.myggchecker.model;

public class CBatchNoInfo {
    private String batch_no;
    private String item_id;
    private String item_code;
    private String item_desc;
    private Integer org_id;
    private String barcode_num;
    private String rak_sementara;
    private String bin_sementara;

    // -------- getter -----------
    public String getBatch_no() {
        return batch_no;
    }
    public String getItem_id() {
        return item_id;
    }
    public String getItem_code() {
        return item_code;
    }
    public String getItem_desc() {
        return item_desc;
    }
    public Integer getOrg_id() {
        return org_id;
    }
    public String getBarcode_num() {
        return barcode_num;
    }
    public String getRak_sementara() {
        return rak_sementara;
    }
    public String getBin_sementara() {
        return bin_sementara;
    }

    // -------- setter -----------
    public void setBatch_no(String batch_no) {
        this.batch_no = batch_no;
    }
    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }
    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }
    public void setItem_desc(String item_desc) {
        this.item_desc = item_desc;
    }
    public void setOrg_id(Integer org_id) {
        this.org_id = org_id;
    }
    public void setBarcode_num(String barcode_num) {
        this.barcode_num = barcode_num;
    }
    public void setRak_sementara(String rak_sementara) {
        this.rak_sementara = rak_sementara;
    }
    public void setBin_sementara(String bin_sementara) {
        this.bin_sementara = bin_sementara;
    }
}
