package com.gudanggaramtbk.myggchecker.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.gudanggaramtbk.myggchecker.activity.BinAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBCodeBinAssign;
import com.gudanggaramtbk.myggchecker.model.CBCodeInfo;
import java.util.List;

public class SoapPutBarcodeToBin {
    private SharedPreferences config;
    private Context context;
    BinAssignActivity pActivity;
    List<CBCodeInfo> TResult;

    // override constructor
    public SoapPutBarcodeToBin(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapPutBarcodeToBin :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapPutBarcodeToBin :: SetContext");
        this.context = context;
    }
    public void setParentActivity(BinAssignActivity pActivity){
        this.pActivity = pActivity;
    }
    public void Assign(final CBCodeBinAssign assignParam) {
        Log.d("[GudangGaram]", "SoapPutBarcodeToBin :: Assign Barcode " + assignParam.getBarcode() + " To Bin >> " + assignParam.getBin());
        try {
            // call WS
            SoapPutBarcodeToBinTaskAsync SoapRequest = new SoapPutBarcodeToBinTaskAsync(new SoapPutBarcodeToBinTaskAsync.SoapPutBarcodeToBinTaskAsyncResponse() {
                @Override
                public void PostSentAction(String RTaskID) {
                }
            });
            //public void setAttribute(Context context, SharedPreferences config, RakAssignActivity pActivity, List<CBinInfo> plist, String prak, String pdeviceid){
            SoapRequest.setAttribute(context, config,  pActivity, assignParam);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapPutBarcodeToBin :: Retrieve Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {
        }
    }
}
