package com.gudanggaramtbk.myggchecker.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.gudanggaramtbk.myggchecker.activity.BinAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBCodeInfo;
import java.util.List;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

public class SoapPopulateInfo {
    private SharedPreferences config;
    private Context           context;
    BinAssignActivity         pActivity;
    List<CBCodeInfo>          TResult;

    // override constructor
    public SoapPopulateInfo(SharedPreferences PConfig){
        Log.d("[GudangGaram]", "SoapPopulateInfo :: Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]", "SoapPopulateInfo :: SetContext");
        this.context = context;
    }
    public void setParentActivity(BinAssignActivity pActivity){
        this.pActivity = pActivity;
    }

    public void Retrieve(final String pBarcode, final String pOrgId) {
        Log.d("[GudangGaram]", "SoapPopulateInfo :: Retrieve Barcode Info >> " + pBarcode);
        try {
            // call WS
            SoapPopulateInfoTaskAsync SoapRequest = new SoapPopulateInfoTaskAsync(new SoapPopulateInfoTaskAsync.SoapPopulateInfoTaskAsyncResponse() {
                @Override
                public void PostSentAction(String result) {
                    if(result.equals("Invalid Barcode")){
                        Toast.makeText(context, "Invalid Barcode", Toast.LENGTH_LONG).show();
                    }
                }
            });
            SoapRequest.setAttribute(context, config,  pActivity, pOrgId, pBarcode);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SoapPopulateInfo :: Retrieve Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
        finally {
        }

    }
}
