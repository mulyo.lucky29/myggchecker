package com.gudanggaramtbk.myggchecker.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.gudanggaramtbk.myggchecker.activity.RakAssignActivity;
import com.gudanggaramtbk.myggchecker.model.CBCodeInfo;
import com.gudanggaramtbk.myggchecker.model.CBinInfo;
import java.util.List;

public class SoapProcessRakAssign  {
        private SharedPreferences config;
        private Context   context;
        RakAssignActivity pActivity;
        List<CBCodeInfo>  TResult;

        // override constructor
        public SoapProcessRakAssign(SharedPreferences PConfig){
            Log.d("[GudangGaram]", "SoapProcessRakAssign :: Constructor");
            this.config = PConfig;
        }
        public void setContext(Context context){
            Log.d("[GudangGaram]", "SoapProcessRakAssign :: SetContext");
            this.context = context;
        }
        public void setParentActivity(RakAssignActivity pActivity){
            this.pActivity = pActivity;
        }
        public void Send(final List<CBinInfo> plist, final String prak, final String pdeviceid) {
            Log.d("[GudangGaram]", "SoapProcessRakAssign :: Assign Bin List To Rak >> " + prak);
            try {
                // call WS
                SoapProcessRakAssignTaskAsync SoapRequest = new SoapProcessRakAssignTaskAsync(new SoapProcessRakAssignTaskAsync.SoapProcessRakAssignTaskAsyncResponse() {
                    @Override
                    public void PostSentAction(String RTaskID) {
                    }
                });
                //public void setAttribute(Context context, SharedPreferences config, RakAssignActivity pActivity, List<CBinInfo> plist, String prak, String pdeviceid){
                SoapRequest.setAttribute(context, config,  pActivity, plist, prak, pdeviceid);
                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                    //work on sgs3 android 4.0.4
                    SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                }
                else {
                    SoapRequest.execute(); // work on sgs2 android 2.3
                }
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "SoapProcessRakAssign :: Retrieve Exception : " + e.getMessage().toString());
                e.printStackTrace();
            }
            finally {
            }
        }
}
